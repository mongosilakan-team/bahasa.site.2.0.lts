<?php

namespace Base\FrameWork\TranslationKit;

use Base\Framework\Constants;
use Base\Models\Custom\ViewTranslationResponse;
use Base\Repositories\ViewTranslationRepository;
use Base\Framework\DevTools\BaseDebugger;

class IndonesianTranslationKit
{
    public static function getTranslation($word, $fromLangageId, $toLanguageId)
    {
        $result = new ViewTranslationResponse();

        $result = ViewTranslationRepository::getSingleWord($word->root, $fromLangageId, $toLanguageId);

        if (!count($result->member)) {

            $possibleInitialChar = array('t');//array('s','m','p','c','t','e','ke','n','k','w');

            switch ($word->prefixType) {
                  case Constants::ParticleType('PrefixType2'):    //eg. menulis = me + (t)ulis
                      foreach ($possibleInitialChar as $initChar) {
                          $result = ViewTranslationRepository::getSingleWord($initChar.$word->root, $fromLangageId, $toLanguageId);
                          if (count($result->member)) {
                              break;
                          }
                      }
                      break;

                  default:
                      break;
              }
        }

        if (isset($result->member[0])) {
            $result->word_type_id = $result->member[0]->word_type_id;
            // $result->wordRootId = $result->member[0]->word_root_id;
            // $result->word_type = $result->member[0]->word_type;
        }

        $result->sourcePrefix = $word->prefix;
        $result->prefixType = $word->prefixType;
        $result->sourceSuffix = $word->suffix;
        $result->suffixType = $word->suffixType;
        // $result->resultRootId = $word->suffixType;
        $result->isModified = true; // the result is modification from system. not pure from database
        $result->isHasParticle = $word->prefix || $word->suffix ? true : false;
        return $result;
    }

    protected static function _adjustPrefixTypeTwo(&$entities)
    {
       $initialChar_me = array('l', 'm', 'n', 'r', 'w', 'y');
       $initialChar_mem = array('b', 'f', 'p', 'v');
       $initialChar_meng = array('a', 'e', 'g', 'h', 'i', 'k', 'o', 'q', 'u', 'x');
       $initialChar_men = array('c', 'd', 'j', 't', 'z');
       $initialChar_meny = array('s');

       foreach ($entities as $entity) {
           $initialChar = substr($entity->result, 0, 1);
               // BaseDebugger::debug($initialChar);die;
               if (in_array($initialChar, $initialChar_me)) {
                   $entity->result = 'me'.$entity->result;
               } elseif (in_array($initialChar, $initialChar_mem)) {
                   if ($initialChar == 'p') { //eg.
                       $entity->result = 'mem'.substr($entity->result, 1);
                   } else {
                       $entity->result = 'mem'.$entity->result;
                   }
               } elseif (in_array($initialChar, $initialChar_meng)) {
                   if ($initialChar == 'k') {
                       $entity->result = 'meng'.substr($entity->result, 1);
                   } else {
                       $entity->result = 'meng'.$entity->result;
                   }
               } elseif (in_array($initialChar, $initialChar_men)) {
                   if ($initialChar == 't') {
                       $entity->result = 'men'.substr($entity->result, 1);
                   } else {
                       $entity->result = 'men'.$entity->result;
                   }
               } elseif (in_array($initialChar, $initialChar_meny)) {
                   $entity->result = 'meny'.substr($entity->result, 1);
               }
       }
           //BaseDebugger::debug(&$entities);die;
           return $entities;
    }
}
