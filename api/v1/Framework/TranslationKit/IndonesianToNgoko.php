<?php

namespace Base\FrameWork\TranslationKit;

use Base\Framework\Constants;
use Base\Models\Custom\ViewTranslationResponse;
use Base\Repositories\ViewTranslationRepository;
use Base\Framework\DevTools\BaseDebugger;
use Base\Framework\TranslationKit\JavaneseTranslationKit;

class IndonesianToNgoko extends JavaneseTranslationKit
{
    // protected static function FROM_LANGUAGE() {
    //     return Constants::LanguageId('id-ID');
    // }
    //
    // protected  static function TO_LANGUAGE() {
    //     return Constants::LanguageId('jv-NG');
    // }

    public static function adjustPrefix(&$words)
    {
        // BaseDebugger::debug($words);
        switch ($words->prefixType) {
            case Constants::ParticleType('PrefixType1'):

                self::_adjustPrefixTypeOne($words->member, $words->sourcePrefix);

                break;

            case Constants::ParticleType('PrefixType2'):

                parent::_adjustPrefixTypeTwo($words->member);

                break;

            case Constants::ParticleType('PrefixType3'):

                parent::_adjustPrefixTypeTwo($words->member);

                self::_adjustPrefixTypeThree($words->member);

                break;

            case Constants::ParticleType('PrefixType4'):

                parent::_adjustPrefixTypeTwo($words->member);

                self::_adjustPrefixTypeFour($words->member, $words->word_type_id);

                break;

            case Constants::ParticleType('PrefixType5'):

                self::_adjustPrefixTypeFive($words->member, $words->word_type_id);

                break;

            case Constants::ParticleType('PrefixType6'):

                self::_adjustPrefixTypeSix($words->member);

                break;

            case Constants::ParticleType('PrefixType7'):

                self::_adjustPrefixTypeSeven($words->member, $words->word_type_id);

                break;

            case Constants::ParticleType('PrefixType8'):

                self::_adjustPrefixTypeEight($words->member, $words->word_type_id);

                break;

            case Constants::ParticleType('PrefixType9'):

                self::_adjustPrefixTypeNine($words->member);

                break;
        }

        return $words;
    }

    public static function adjustSuffix(&$words)
    {
        switch ($words->suffixType) {
            case Constants::ParticleType('SuffixType1'):
                self::_adjustSuffixTypeOne($words->member);
                break;

            case Constants::ParticleType('SuffixType2'):
                self::_adjustSuffixTypeTwo($words->member);
                break;

            case Constants::ParticleType('SuffixType3'):
                self::_adjustSuffixAdditionalTypeThree($words->member, $words->sourceSuffix);
                break;

            case Constants::ParticleType('SuffixType4'):
                parent::_adjustSuffixTypeFour($words->member, $words->word_type_id);
                break;

            case Constants::ParticleType('SuffixType5'):
                self::_adjustSuffixTypeFive($words->member, $words->prefixType, $words->sourceSuffix);
                break;

            case Constants::ParticleType('SuffixType6'):
                self::_adjustSuffixTypeSix($words->member, $words->sourceSuffix);
                break;

            case Constants::ParticleType('SuffixType7'):
                self::_adjustSuffixTypeSeven($words->member);
                break;

            case Constants::ParticleType('SuffixType8'):
                self::_adjustSuffixTypeEight($words->member);
                break;

            case Constants::ParticleType('SuffixType9'):
                parent::_adjustSuffixTypeNine($words->member);
                break;

            case Constants::ParticleType('SuffixType10'):
                parent::_adjustSuffixTypeTen($words->member);
                break;

            default:
                break;
        }

        return $words;
    }

    //region private method
    //region suffix
    private static function _adjustSuffixTypeOne(&$entities)
    {
        $endChar_ake = array('k', 'm', 'l', 'g', 'h', 'p', 't');

        $endChar_na = array('ik', 'ng');

        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);

            $endChar_2 = substr($entity->result, strlen($entity->result) - 2, 2);

            if (in_array($endChar_2, $endChar_na)) {
                $entity->result .= 'na';
                continue;
            }

            if (in_array($endChar, $endChar_ake)) {
                $entity->result .= 'ake';
                continue;
            }

            switch ($endChar) {
                case 'u':

                    $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);

                    $entity->result .= 'okake';

                    break;

                case 'a':

                    $entity->result .= 'kne';

                    break;

                case 'e':

                    $entity->result .= 'kne';

                    break;

                case 'i':

                    $entity->result = substr($entity->result, 0, -1);

                    $entity->result .= 'ekne';

                    break;

                default:

                    $entity->result .= 'ke';

                    break;
            }
        }

        return $entities;
    }

    protected static function _adjustSuffixAdditionalTypeThree(&$entities, $suffix)
    {
        parent::_adjustSuffixTypeThree($entities);

        foreach ($entities as $entity) {
            switch ($suffix) {
                case 'imu':
                    $entity->result .= ' kowe';
                    break;
                case 'iku':
                    $entity->result .= ' aku';
                    break;
                default:
                    break;
            }
        }

        return $entities;
    }

    private static function _adjustSuffixTypeFive(&$entities, $prefixType, $suffix)
    {
        $prefixWord = '';
        $isPrefixTypeTwo = $prefixType == Constants::ParticleType('PrefixType2');

        foreach ($entities as $entity) {
            if ($suffix == 'mu') {
                $entity->result .= $isPrefixTypeTwo ? ' kowe' : 'mu';
            } elseif ($suffix == 'ku') {
                $entity->result .= $isPrefixTypeTwo ? ' aku' : 'ku';
            }
        }

        return $entities;
    }

    private static function _adjustSuffixTypeSix(&$entities, $suffix)
    {
        $endChar_ne = array('e', 'a', 'i', 'u', 'o');

        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);

            if (in_array($endChar, $endChar_ne)) {
                $entity->result .= 'ne';
            } else {
                $entity->result .= 'e';
            }
        }

        return $entities;
    }

    private static function _adjustSuffixTypeSeven(&$entities)
    {
        $endChar_ake = array('k', 'm', 'l', 'g', 'h');

        $endChar_na = array('ik'); //still any possibility to add more

        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);
            $endChar2 = substr($entity->result, strlen($entity->result) - 2, 2);
            if (in_array($endChar2, $endChar_na)) {
                $entity->result .= 'ane';
                continue;
            }

            switch ($endChar) {
                case 'u'    :
                    $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);
                    $entity->result .= 'onane';
                    break;

                case 'a'    :
                    $entity->result .= 'nane';
                    break;

                case 'i'    :
                    $entity->result = substr($entity->result, 0, -1);
                    $entity->result .= 'enane';
                    break;

                default     :
                    $entity->result .= 'ane';
                    break;
            }
        }

        return $entities;
    }

    private static function _adjustSuffixTypeEight(&$entities)
    {
        $endChar_na = array('ik'); //possibility to add more

        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);
            $endChar_2 = substr($entity->result, strlen($entity->result) - 2, 2);
            if (in_array($endChar_2, $endChar_na)) {
                $entity->result .= 'na';
                continue;
            }
            switch ($endChar) {
                case 'u'    :
                    $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);
                    $entity->result  .= 'onana';
                    break;

                case 'a'    :
                    $entity->result  .= 'nana';
                    break;

                case 'o'    :
                    $entity->result  .= 'nana';
                    break;

                case 'e'    :
                    $entity->result  .= 'nana';
                    break;

                case 'i'    :
                    $entity->result = substr($entity->result, 0, -1);
                    $entity->result  .= 'enana';
                    break;
                default     :
                    $entity->result  .= 'ana';
                    break;
            }
        }

        return $entities;
    }
    //endregion suffix

    //region prefix
    private static function _adjustPrefixTypeOne(&$entities, $prefix)
    {
        foreach ($entities as $entity) {
            switch ($prefix) {
                case 'di':

                    $entity->result = 'di'.$entity->result;

                    break;

                case 'se':
                    $entity->result = 'sa'.$entity->result;

                    break;

                case 'ku':

                    $entity->result = 'dak'.$entity->result;

                    break;

                case 'kau':

                    $entity->result = 'kok'.$entity->result;

                    break;

                case 'menge':

                    $entity->result = 'nge'.$entity->result;

                    break;

                case 'penge':

                    $entity->result = 'sing '.'nge'.$entity->result;

                    break;

                default:

                    break;
            }
        }

        return $entities;
    }

    private static function _adjustPrefixTypeThree(&$entities)
    {
        foreach ($entities as $entity) {
            $entity->result = 'pa'.$entity->result;
        }

        return $entities;
    }

    private static function _adjustPrefixTypeFour(&$entities, $word_type_id)
    {
        if ($word_type_id == 'sifat') {
            var_dump('tbd');
            die;

            //return $this->cek_akhiran_kan($tembung_golek);
        }

        return $entities;
    }

    private static function _adjustPrefixTypeFive(&$entities, $word_type_id)
    {
        switch ($word_type_id) {
            case Constants::WordTypes('Verb'):
            case Constants::WordTypes('Number'):

                //eg. berbohong
                    parent::_adjustPrefixTypeTwo($entities);

                break;

            case Constants::WordTypes('Adjective'):
            case Constants::WordTypes('Pronoun'):
            case Constants::WordTypes('Noun'):

                //eg. bernilai
                foreach ($entities as $entity) {
                    $entity->result = 'nduwe '.$entity->result;
                }

                break;

            default:

                break;
        }

        return $entities;
    }

    private static function _adjustPrefixTypeSix(&$entities)
    {
        $initialChar_k = array('u', 'e');

        foreach ($entities as $entity) {
            $initialChar = substr($entity->result, 0, 1);

            if (in_array($initialChar, $initialChar_k)) {
                $entity->result = 'k'.$entity->result;
            } else {
                $entity->result = 'ke'.$entity->result;
            }
        }

        return $entities;
    }

    private static function _adjustPrefixTypeSeven(&$entities, $word_type_id)
    {
        switch ($word_type_id) {
            case Constants::WordTypes('Adjective'):

                //return $this->cek_akhiran_kan($tembung_golek);
                BaseDebugger::debug("Tobe develope");
                die;
                break;

            case Constants::WordTypes('Verb'):
            case Constants::WordTypes('Number'):
            case Constants::WordTypes('Pronoun'):
            case Constants::WordTypes('Noun'):

                foreach ($entities as $entity) {
                    $entity->result = 'saben '.$entity->result;
                }

                break;

            default:
                break;
        }

        return $entities;
    }

    private static function _adjustPrefixTypeEight(&$entities, $word_type_id)
    {
        switch ($word_type_id) {
            case Constants::WordTypes('Adjective'):

                foreach ($entities as $entity) {
                    $entity->result = 'paling '.$entity->result;
                }

                break;

            case Constants::WordTypes('Verb'):

                self::_adjustPrefixTypeSix($entities);

                break;

            case Constants::WordTypes('Number'):
            case Constants::WordTypes('Pronoun'):
            case Constants::WordTypes('Noun'):
            default:

                break;
        }

        return $entities;
    }

    private static function _adjustPrefixTypeNine(&$entities)
    {
        foreach ($entities as $entity) {
            var_dump('tbd');
            die;

            // return "di".$this->cek_akhiran_kan($tembung_golek);
            $entity->result = 'di'.$entity->result;
        }

        return $entities;
    }

    //endregion prefix
    //endregion private method
}
