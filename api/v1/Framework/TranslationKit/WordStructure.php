<?php

namespace Base\FrameWork\TranslationKit;

/**
 * WordStructure class.
 */
class WordStructure
{
    public function __construct($original = null)
    {
        $this->original = $original;
    }

    public $original;

    public $root;

    public $prefix;

    public $prefixType;

    public $suffix;
    
    public $suffixType;
}
