<?php

namespace Base\FrameWork\TranslationKit;

use Base\Framework\Constants;
use Base\Models\Custom\ViewTranslationResponse;
use Base\Repositories\ViewTranslationRepository;
use Base\Framework\DevTools\BaseDebugger;

class KramaToIndonesian
{
    public static function getTranslation($word, $fromLangageId, $toLanguageId)
    {
        $result = new ViewTranslationResponse();

        $result = ViewTranslationRepository::getSingleWord($word->root, $fromLangageId, $toLanguageId);
        if (!count($result->member)) {

            $possibleInitialChar = array('s', 'm', 'p', 't', 'k', 'e', 'ke', 'n','w');

            switch ($word->prefixType) {
                  case Constants::ParticleType('PrefixType2'): // nyapu => s + apu
                      foreach ($possibleInitialChar as $initChar) {
                          $result = ViewTranslationRepository::getSingleWord($initChar.$word->root, $fromLangageId, $toLanguageId);
                          if (count($result->member)) {
                              break;
                          }
                      }
                      break;

                  default:
                      break;
              }
        }

        if (isset($result->member[0])) {
            $result->word_type_id = $result->member[0]->word_type_id;
            // $result->wordRootId = $result->member[0]->word_root_id;
            // $result->word_type = $result->member[0]->word_type;
        }

        $result->sourcePrefix = $word->prefix;
        $result->prefixType = $word->prefixType;
        $result->sourceSuffix = $word->suffix;
        $result->suffixType = $word->suffixType;
        // $result->resultRootId = $word->suffixType;
        $result->isModified = true; // the result is modification from system. not pure from database
        $result->isHasParticle = $word->prefix || $word->suffix ? true : false;
        return $result;
    }


    public static function adjustSuffix(&$words)
    {
        switch ($words->suffixType) {
            case Constants::ParticleType('SuffixType1'):
                self::_adjustSuffixTypeOne($words->member, $words->sourceSuffix);
                break;

            case Constants::ParticleType('SuffixType2'):
                self::_adjustSuffixTypeTwo($words->member, $words->sourceSuffix, $words->sourcePrefix);
                break;

            case Constants::ParticleType('SuffixType3'):
                self::_adjustSuffixTypeThree($words->member);
                break;

            case Constants::ParticleType('SuffixType4'):
                self::_adjustSuffixTypeFour($words->member, $words->sourcePrefix);
                break;

            case Constants::ParticleType('SuffixType5'):
                self::_adjustSuffixTypeFive($words->member);
                break;
            default:
                break;
        }

        return $words;
    }

    private static function _adjustSuffixTypeFive(&$entities)
    {
        foreach ($entities as $entity) {
            if(substr($entity->result,-1) == "i")
            {
                $entity->result .= "nya";
            }
            else $entity->result .= "anya"; // gawanane -> bawaanya
        }
    }

    private static function _adjustSuffixTypeFour(&$entities, $prefix)
    {
        foreach ($entities as $entity) {
            if ($prefix == 'di')
            {
                if(substr($entity->result,-1) == "i") $entity->result .= "pun";
                else $entity->result .= "ipun";
            }
            elseif(substr($entity->result,-1) == "i") $entity->result .= "lah";
            else $entity->result .= "ilah";
        }
    }

    private static function _adjustSuffixTypeThree(&$entities)
    {
        // BaseDebugger::debug($entities);die;
        foreach ($entities as $entity) {
            // TODO : need recheck on v1
            $entity->result .= "an";
        }
    }

    private static function _adjustSuffixTypeTwo(&$entities, $suffix, $prefix)
    {
        foreach ($entities as $entity) {
            switch ($suffix) {
                case 'a':
                    if ($prefix == "di") $entity->result .= "pun"; //digawaa -> dibawapun
                    else $entity->result .= "lah"; // gawaa -> bawalah
                    break;
                case 'i':
                    if(substr($entity->result,-1) != "i") {
                        $entity->result =  $entity->result."i";
                    }
                    break;
                case 'nen':
                    if($prefix == "te") $entity->result =  "ke".substr($entity->result,3)."an"; // kedawanen
                    else $entity->result .= "lah"; // ambunen
                    break;
                case 'en':
                    if($prefix == "te") $entity->result = "ke".substr($tembung_golek,3)."an"; // kecendheken
                    $entity->result = $entity->result."lah";
                    break;
                case 'ni':
                    if(substr($entity->result,-1) != "i") {
                        return $entity->result."i";
                    }
                    break;
                case 'a':
                    if ($prefix == "di") $entity->result = $entity->result."pun"; //digawaa -> dibawapun
                    $entity->result = $entity->result."lah"; // gawaa -> bawalah
                    break;
                case 'oni':
                    if(substr($entity->result,-1) != "i") {
                        $entity->result = $entity->result."i";
                    }
                    break;
                case 'eni':
                    if(substr($entity->result,-1) != "i") {
                        $entity->result = $entity->result."i";
                    }
                    break;
                default:
                    break;
            }
        }

        return $entities;
    }

    private static function _adjustSuffixTypeOne(&$entities, $suffix)
    {
        foreach ($entities as $entity) {
            switch ($suffix) {
                case 'e':
                case 'ne':
                    $entity->result = $entity->result.'nya';
                    break;
                case 'ke':
                case 'na':
                case 'kake':
                case 'ake':
                case 'okake':
                case 'okne':
                case 'kne':
                case 'ekne':
                case 'enake':
                case 'ekake':
                    $entity->result = $entity->result.'kan';
                    break;
                case 'on':
                    $entity->result = $entity->result.'an';
                    break;
                case 'ku':
                    $entity->result = $entity->result.'ku';
                    break;
                case 'mu':
                    $entity->result = $entity->result.'mu';
                    break;
                case 'okna':
                case 'kna':
                case 'ekna':
                    $entity->result = $entity->result.'kanlah';
                    break;
                default:
                    break;
            }
        }

        return $entities;
    }

    public static function adjustPrefix(&$words)
    {
      // BaseDebugger::debug($words->sourcePrefix);die;
        switch ($words->prefixType) {
            case Constants::ParticleType('PrefixType1'):
            // BaseDebugger::debug($words->prefixType);die;
                self::_adjustPrefixTypeOne($words->member, $words->sourcePrefix);
                break;

            case Constants::ParticleType('PrefixType2'):

                self::_adjustPrefixTypeTwo($words->member);

                break;

            case Constants::ParticleType('PrefixType3'):

                self::_adjustPrefixTypeTwo($words->member);

                self::_adjustPrefixTypeThree($words->member);

                break;
        }
        // BaseDebugger::debug($words);die;
        return $words;
    }


    private static function _adjustPrefixTypeThree(&$entities)
    {
        foreach ($entities as $entity) {
            $entity->result = 'pe'.substr($entity->result,2);
        }
        return $entities;
    }

    private static function _adjustPrefixTypeTwo(&$entities)
    {
        $initialChar_me = array('l','m','n','r','w','y');
        $initialChar_mem = array('b','f','p','v');
        $initialChar_meng = array('a','e','g','h','i','k','o','q','u','x');
        $initialChar_men = array('c','d','j','t','z');
        $initialChar_meny = array('s');

        foreach ($entities as $entity) {

            $initialChar = substr($entity->result, 0, 1);
            // BaseDebugger::debug($initialChar);die;
            if (in_array($initialChar, $initialChar_me))
            {
                $entity->result = 'me'.$entity->result;
            }
            elseif (in_array($initialChar, $initialChar_mem))
            {
                if($initialChar == 'p') //eg.
                {
                    $entity->result =  "mem".substr($entity->result, 1);
                }
                else {
                    $entity->result = "mem".$entity->result;
                }
            }
            elseif (in_array($initialChar, $initialChar_meng))
            {
                if($initialChar=='k')
                {
                    $entity->result = "meng".substr($entity->result, 1);
                }
                else {
                    $entity->result = "meng".$entity->result;
                }
            }
            elseif (in_array($initialChar, $initialChar_men))
            {
                if($initialChar=='t')
                {
                    $entity->result = "men".substr($entity->result, 1);
                } else {
                    $entity->result = "men".$entity->result;
                }
            }
            elseif (in_array($initialChar, $initialChar_meny))
            {
                    $entity->result = "meny".substr($entity->result, 1);
            }
        }
        //BaseDebugger::debug(&$entities);die;
        return $entities;
    }

    private static function _adjustPrefixTypeOne(&$entities, $prefix)
    {
        foreach ($entities as $entity) {
            switch ($prefix) {
                case 'di':
                    $entity->result = 'di'.$entity->result;
                    break;
                case 'sa':
                case 'sak':
                case 'se':
                    $entity->result = 'se'.$entity->result;
                    break;
                case 'ke':
                    $entity->result = 'ter'.$entity->result;
                    break;
                case 'kok':
                    $entity->result = 'kau '.$entity->result;
                    break;
                case 'dak':
                    $entity->result = 'ku'.$entity->result;
                    break;
                default:
                    $entity->result = $prefix.$entity->result;
                    break;
            }
        }

        return $entities;
    }
}
