<?php

namespace Base\FrameWork\TranslationKit;

use Base\Framework\Constants;
use Base\Models\Custom\ViewTranslationResponse;
use Base\Repositories\ViewTranslationRepository;
use Base\Framework\DevTools\BaseDebugger;

class JavaneseTranslationKit
{
    public static function getTranslation($word, $fromLangageId, $toLanguageId)
    {
        $result = new ViewTranslationResponse();

        $result = ViewTranslationRepository::getSingleWord($word->root, $fromLangageId, $toLanguageId);

        if (!count($result->member)) { // possible init char
            $possibleInitialChar = array('s','m','p','c','t','e','ke','n','k','w');

            switch ($word->prefixType) {
                  case Constants::ParticleType('PrefixType2'):
                      // case : nyapu = ny + apu => ny + sapu
                      foreach ($possibleInitialChar as $initChar) {
                          $result = ViewTranslationRepository::getSingleWord($initChar.$word->root, $fromLangageId, $toLanguageId);
                          if (count($result->member)) {
                              break;
                          }
                      }
                      break;

                  default:
                      break;
              }
        }

        if (!count($result->member)) { // possible end char
            switch (substr($word->root,-1)) {
                case "o":
                    $result = ViewTranslationRepository::getSingleWord(substr($word->root,0,-1).'u', $fromLangageId, $toLanguageId);
                    if (count($result->member)) {
                      break;
                    }
                    break;
                case "e":
                    $result = ViewTranslationRepository::getSingleWord(substr($word->root,0,-1).'i', $fromLangageId, $toLanguageId);
                    if (count($result->member)) {
                      break;
                    }
                    break;
                default:
                    break;
            }
        }

        if (isset($result->member[0])) {
            $result->word_type_id = $result->member[0]->word_type_id;
        }

        $result->sourcePrefix = $word->prefix;
        $result->prefixType = $word->prefixType;
        $result->sourceSuffix = $word->suffix;
        $result->suffixType = $word->suffixType;
        $result->isModified = true; // the result is modification from system. not pure from database
        $result->isHasParticle = $word->prefix || $word->suffix ? true : false;

        return $result;
    }

    protected static function _adjustSuffixTypeTen(&$entities)
    {
        $endChar_ana = array('a', 'o', 'e'); //bawakanpun, pakaikanpun, buatkanpun,
        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);

            if (in_array($endChar, $endChar_ana)) {
                $entity->result .= 'nana';
                continue;
            }

            switch ($endChar) {
                case 'u'    :
                    $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);
                    $entity->result .= 'onana';
                    break;

                case 'i' :
                    $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);
                    $entity->result .= 'enana'; //ulangkanpun
                    break;

                default;
                    $entity->result .= 'ana';
                    break;
            }
        }

        // return $entities;
    }

    protected static function _adjustSuffixTypeNine(&$entities)
    {
        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);
            // if ($endChar == 'u') {
            //     $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);
            //     $entity->result .= 'okna'; // sapupun -> sapokna ???
            // } else {
                $entity->result .= 'a'; // eg. dipanahpun -> dipanaha
            // }
        }

        // return $entities;
    }

    protected static function _adjustSuffixTypeFour(&$entities, $word_type_id)
    {
        if ($word_type_id != Constants::WordTypes('Pronoun')) {
            foreach ($entities as $entity) {
                $entity->result .= 'a';
            }
        }

        // return $entities;
    }

    protected static function _adjustSuffixTypeThree(&$entities)
    {
        $endChar_ni = array('e', 'a', 'o');

        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);

            if (in_array($endChar, $endChar_ni)) {
                $entity->result .= 'ni';
            } elseif ($endChar == 'i') {
                $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);

                $entity->result .= 'eni';
            } elseif ($endChar == 'u') {
                $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);

                $entity->result .= 'oni';
            } else {
                $entity->result .= 'i';
            }
        }
    }

    protected static function _adjustPrefixTypeTwo(&$entities)
    {
        $initialCharNeutral = array('n', 'f', 'h', 'q', 'v', 'x', 'y', 'z', 'ma', 'mo', 'mi', 'm');

        $initialChar_ng = array('a', 'e', 'g', 'i', 'k', 'l', 'o', 'r', 'u');

        $initialChar_m = array('b', 'w', 'p');

        $initialChar_ny = array('c', 's');

        $initialChar_n = array('d', 'j', 't');

        foreach ($entities as $entity) {

            $initialChar = substr($entity->result, 0, 1);

            $initialChar_2 = substr($entity->result, 0, 2);
            // if (in_array($initialChar, $initialCharNeutral) or in_array($initialChar_2, $initialCharNeutral)) {
            //     $entity->result = $entity->result;
            // } else
            if (in_array($initialChar, $initialChar_ng)) {

                //eg. mengeluarkan
                if ($initialChar == 'k' or $initialChar == 'm') {
                    $entity->result = substr($entity->result, 1);
                }

                $entity->result = 'ng'.$entity->result;
            } elseif (in_array($initialChar, $initialChar_m)) {
                if ($initialChar != 'b') {
                    $entity->result = substr($entity->result, 1);
                }

                $entity->result = 'm'.$entity->result;
            } elseif (in_array($initialChar, $initialChar_ny)) {
                $entity->result = substr($entity->result, 1);

                $entity->result = 'ny'.$entity->result;
            } elseif (in_array($initialChar, $initialChar_n)) {
                if ($initialChar == 't') {
                    $entity->result = substr($entity->result, 1);
                }

                $entity->result = 'n'.$entity->result;
            }
        }
    }

    private static function _adjustPrefixTypeSix(&$entities)
    {
        $initialChar_k = array('u', 'e');

        foreach ($entities as $entity) {
            $initialChar = substr($entity->result, 0, 1);

            if (in_array($initialChar, $initialChar_k)) {
                $entity->result = 'k'.$entity->result;
            } else {
                $entity->result = 'ke'.$entity->result;
            }
        }
    }

    protected static function _adjustSuffixTypeTwo(&$entities)
    {
        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);

            if ($endChar == 'o') {
                $entity->result .= 'nan';
            } elseif ($endChar == 'a' || $endChar == 'e') {
                $entity->result .= 'n';
            } elseif ($endChar == 'i') {
                $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);
                $entity->result .= 'en';
            } elseif ($endChar == 'u') {
                $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);
                $entity->result .= 'on';
            } else {
                $entity->result .= 'an';
            }
        }
    }
}
