<?php

namespace Base\FrameWork\TranslationKit;

use Base\Framework\Constants;
use Base\Framework\DevTools\BaseDebugger;
use Base\Framework\TranslationKit\IndonesianTranslationKit;

class NgokoToIndonesian extends IndonesianTranslationKit
{
    // protected static function FROM_LANGUAGE() {
    //     return Constants::LanguageId('jv-NG');
    // }
    //
    // protected  static function TO_LANGUAGE() {
    //     return Constants::LanguageId('id-ID');
    // }

    public static function adjustSuffix(&$words)
    {
        switch ($words->suffixType) {
                case Constants::ParticleType('SuffixType1'):
                    self::_adjustSuffixTypeOne($words->member, $words->sourceSuffix);
                    break;

                case Constants::ParticleType('SuffixType2'):
                    self::_adjustSuffixTypeTwo($words->member, $words->sourceSuffix, $words->sourcePrefix);
                    break;

                case Constants::ParticleType('SuffixType3'):
                    self::_adjustSuffixTypeThree($words->member);
                    break;

                case Constants::ParticleType('SuffixType4'):
                    self::_adjustSuffixTypeFour($words->member, $words->sourcePrefix);
                    break;

                case Constants::ParticleType('SuffixType5'):
                    self::_adjustSuffixTypeFive($words->member);
                    break;
                default:
                    break;
            }

        return $words;
    }

    private static function _adjustSuffixTypeFive(&$entities)
    {
        foreach ($entities as $entity) {
            if (substr($entity->result, -1) == 'i') {
                $entity->result .= 'nya';
            } else {
                $entity->result .= 'anya';
            } // gawanane -> bawaanya
        }
    }

    private static function _adjustSuffixTypeFour(&$entities, $prefix)
    {
        foreach ($entities as $entity) {
            if ($prefix == 'di') {
                if (substr($entity->result, -1) == 'i') {
                    $entity->result .= 'pun';
                } else {
                    $entity->result .= 'ipun';
                }
            } elseif (substr($entity->result, -1) == 'i') {
                $entity->result .= 'lah';
            } else {
                $entity->result .= 'ilah';
            }
        }
    }

    private static function _adjustSuffixTypeThree(&$entities)
    {
        // BaseDebugger::debug($entities);die;
            foreach ($entities as $entity) {
                // TODO : need recheck on v1
                $entity->result .= 'an';
            }
    }

    private static function _adjustSuffixTypeTwo(&$entities, $suffix, $prefix)
    {
        foreach ($entities as $entity) {
            switch ($suffix) {
                    case 'a':
                        if ($prefix == 'di') {
                            $entity->result .= 'pun';
                        } //digawaa -> dibawapun
                        else {
                            $entity->result .= 'lah';
                        } // gawaa -> bawalah
                        break;
                    case 'i':
                        if (substr($entity->result, -1) != 'i') {
                            $entity->result = $entity->result.'i';
                        }
                        break;
                    case 'nen':
                        if ($prefix == 'te') {
                            $entity->result = 'ke'.substr($entity->result, 3).'an';
                        } // kedawanen
                        else {
                            $entity->result .= 'lah';
                        } // ambunen
                        break;
                    case 'en':
                        if ($prefix == 'te') {
                            $entity->result = 'ke'.substr($tembung_golek, 3).'an';
                        } // kecendheken
                        $entity->result = $entity->result.'lah';
                        break;
                    case 'ni':
                        if (substr($entity->result, -1) != 'i') {
                            return $entity->result.'i';
                        }
                        break;
                    case 'a':
                        if ($prefix == 'di') {
                            $entity->result = $entity->result.'pun';
                        } //digawaa -> dibawapun
                        $entity->result = $entity->result.'lah'; // gawaa -> bawalah
                        break;
                    case 'oni':
                        if (substr($entity->result, -1) != 'i') {
                            $entity->result = $entity->result.'i';
                        }
                        break;
                    case 'eni':
                        if (substr($entity->result, -1) != 'i') {
                            $entity->result = $entity->result.'i';
                        }
                        break;
                    default:
                        break;
                }
        }

        return $entities;
    }

    private static function _adjustSuffixTypeOne(&$entities, $suffix)
    {
        foreach ($entities as $entity) {
            switch ($suffix) {
                    case 'e':
                    case 'ne':
                        $entity->result = $entity->result.'nya';
                        break;
                    case 'ke':
                    case 'na':
                    case 'kake':
                    case 'ake':
                    case 'okake':
                    case 'okne':
                    case 'kne':
                    case 'ekne':
                    case 'enake':
                    case 'ekake':
                        $entity->result = $entity->result.'kan';
                        break;
                    case 'on':
                        $entity->result = $entity->result.'an';
                        break;
                    case 'ku':
                        $entity->result = $entity->result.'ku';
                        break;
                    case 'mu':
                        $entity->result = $entity->result.'mu';
                        break;
                    case 'okna':
                    case 'kna':
                    case 'ekna':
                        $entity->result = $entity->result.'kanlah';
                        break;
                    default:
                        break;
                }
        }

        return $entities;
    }

    public static function adjustPrefix(&$words)
    {
        // BaseDebugger::debug($words->sourcePrefix);die;
            switch ($words->prefixType) {
                case Constants::ParticleType('PrefixType1'):
                // BaseDebugger::debug($words->prefixType);die;
                    self::_adjustPrefixTypeOne($words->member, $words->sourcePrefix);
                    break;

                case Constants::ParticleType('PrefixType2'):

                    parent::_adjustPrefixTypeTwo($words->member);

                    break;

                case Constants::ParticleType('PrefixType3'):

                    parent::_adjustPrefixTypeTwo($words->member);

                    self::_adjustPrefixTypeThree($words->member);

                    break;
            }
            // BaseDebugger::debug($words);die;
            return $words;
    }

    private static function _adjustPrefixTypeThree(&$entities)
    {
        foreach ($entities as $entity) {
            $entity->result = 'pe'.substr($entity->result, 2);
        }

        return $entities;
    }

    private static function _adjustPrefixTypeOne(&$entities, $prefix)
    {
        foreach ($entities as $entity) {
            switch ($prefix) {
                    case 'di':
                        $entity->result = 'di'.$entity->result;
                        break;
                    case 'sa':
                    case 'sak':
                    case 'se':
                        $entity->result = 'se'.$entity->result;
                        break;
                    case 'ke':
                        $entity->result = 'ter'.$entity->result;
                        break;
                    case 'kok':
                        $entity->result = 'kau '.$entity->result;
                        break;
                    case 'dak':
                        $entity->result = 'ku'.$entity->result;
                        break;
                    default:
                        $entity->result = $prefix.$entity->result;
                        break;
                }
        }

        return $entities;
    }
}
