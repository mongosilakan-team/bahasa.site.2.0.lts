<?php

namespace Base\FrameWork\TranslationKit;

use Base\Framework\Constants;
use Base\Models\Custom\ViewTranslationResponse;
use Base\Repositories\ViewTranslationRepository;
use Base\Framework\DevTools\BaseDebugger;
use Base\Framework\TranslationKit\JavaneseTranslationKit;

class IndonesianToKrama extends JavaneseTranslationKit
{
    // protected static function FROM_LANGUAGE() {
    //     return Constants::LanguageId('id-ID');
    // }
    //
    // protected  static function TO_LANGUAGE() {
    //     return Constants::LanguageId('jv-KR');
    // }

    const Krama_Saya = "kula";
    const KramaInggil_Kamu = "panjenengan";
    const Krama_Kamu = "sampeyan";

    public static function adjustPrefix(&$words)
    {
        // BaseDebugger::debug($words);die;
        switch ($words->prefixType) {
            case Constants::ParticleType('PrefixType1'):

                self::_adjustPrefixTypeOne($words->member, $words->sourcePrefix);

                break;

            case Constants::ParticleType('PrefixType2'):

                parent::_adjustPrefixTypeTwo($words->member);

                break;

            case Constants::ParticleType('PrefixType3'):

                parent::_adjustPrefixTypeTwo($words->member);

                self::_adjustPrefixTypeThree($words->member);

                break;

            case Constants::ParticleType('PrefixType4'):

                parent::_adjustPrefixTypeTwo($words->member);

                self::_adjustPrefixTypeFour($words->member, $words->word_type_id);

                break;

            case Constants::ParticleType('PrefixType5'):

                self::_adjustPrefixTypeFive($words->member, $words->word_type_id);

                break;

            case Constants::ParticleType('PrefixType6'):

                self::_adjustPrefixJavaneseTypeSix($words->member);

                break;

            case Constants::ParticleType('PrefixType7'):

                self::_adjustPrefixTypeSeven($words->member, $words->word_type_id);

                break;

            case Constants::ParticleType('PrefixType8'):

                self::_adjustPrefixTypeEight($words->member, $words->word_type_id);

                break;

            case Constants::ParticleType('PrefixType9'):

                self::_adjustPrefixTypeNine($words->member);

                break;
        }

        return $words;
    }

    public static function adjustSuffix(&$words)
    {
        switch ($words->suffixType) {
            case Constants::ParticleType('SuffixType1'):
                self::_adjustSuffixTypeOne($words->member);
                break;

            case Constants::ParticleType('SuffixType2'):
                parent::_adjustSuffixTypeTwo($words->member);
                break;

            case Constants::ParticleType('SuffixType3'):
                self::_adjustSuffixAdditionalTypeThree($words->member, $words->sourceSuffix);
                break;

            case Constants::ParticleType('SuffixType4'):
                parent::_adjustSuffixTypeFour($words->member, $words->word_type_id);
                break;

            case Constants::ParticleType('SuffixType5'):
                self::_adjustSuffixTypeFive($words->member, $words->prefixType, $words->sourceSuffix);
                break;

            case Constants::ParticleType('SuffixType6'):
                self::_adjustSuffixTypeSix($words->member, $words->sourceSuffix);
                break;

            case Constants::ParticleType('SuffixType7'):
                self::_adjustSuffixTypeSeven($words->member);
                break;

            case Constants::ParticleType('SuffixType8'):
                self::_adjustSuffixTypeEight($words->member);
                break;

            case Constants::ParticleType('SuffixType9'):
                parent::_adjustSuffixTypeNine($words->member);
                break;

            case Constants::ParticleType('SuffixType10'):
                parent::_adjustSuffixTypeTen($words->member);
                break;

            default:
                break;
        }

        return $words;
    }

    //region private method
    //region suffix
    private static function _adjustSuffixTypeOne(&$entities)
    {
        $endCharForKaken = array('a','i','e');
        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);
            if(in_array($endChar, $endCharForKaken)) {
                $entity->result .= "kaken";
            }
            else if($endChar == "u")
            {
                $entity->result = substr($entity->result,0,strlen($entity->result)-1)."okaken";
            }
            else  $entity->result .= "aken";
        }
    }

    protected static function _adjustSuffixAdditionalTypeThree(&$entities, $suffix)
    {
        parent::_adjustSuffixTypeThree($entities);

        foreach ($entities as $entity) {
            switch ($suffix) {
                case 'imu':
                    $entity->result .= $entity->result_language_id == Constants::LanguageId('jv-KR')? ' '. self::Krama_Kamu : ' '. self::KramaInggil_Kamu;
                    break;
                case 'iku':
                    // BaseDebugger::debug('asf');die;
                    $entity->result .= ' '.self::Krama_Saya;
                    break;
                default:
                    break;
            }
        }

        // return $entities;
    }

    private static function _adjustSuffixTypeFive(&$entities, $prefixType, $suffix)
    {
        $prefixWord = '';
        $isPrefixTypeTwo = $prefixType == Constants::ParticleType('PrefixType2');

        foreach ($entities as $entity) {
            if ($suffix == 'mu') {
                $entity->result .= $isPrefixTypeTwo ? ' sampeyan' : 'mu';
            } elseif ($suffix == 'ku') {
                switch ($entity->result_language_id) {
                    case Constants::LanguageId('jv-KR'):
                        $entity->result .= $isPrefixTypeTwo ? ' '.self::Krama_Saya : 'ku';
                        break;
                    case Constants::LanguageId('jv-KI'):
                        $entity->result .= ' '.self::Krama_Saya;
                        break;
                    default:
                        # code...
                        break;
                }
            }
        }

        // return $entities;
    }

    private static function _adjustSuffixTypeSix(&$entities, $suffix)
    {
        $endChar_ne = array('e', 'a', 'i', 'u', 'o');

        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);

            if (in_array($endChar, $endChar_ne)) {
                $entity->result .= 'nipun';
            } else {
                $entity->result .= 'ipun';
            }
        }

        // return $entities;
    }

    private static function _adjustSuffixTypeSeven(&$entities)
    {
        $endChar_ake = array('k', 'm', 'l', 'g', 'h');

        $endChar_na = array('ik'); //still any possibility to add more

        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);
            $endChar2 = substr($entity->result, strlen($entity->result) - 2, 2);
            if (in_array($endChar2, $endChar_na)) {
                $entity->result .= 'anipun';
                continue;
            }

            switch ($endChar) {
                case 'u'    :
                    $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);
                    $entity->result .= 'onanipun';
                    break;

                case 'a'    :
                    $entity->result .= 'nanipun';
                    break;

                case 'i'    :
                    $entity->result = substr($entity->result, 0, -1);
                    $entity->result .= 'enanipun';
                    break;

                default     :
                    $entity->result .= 'anipun';
                    break;
            }
        }

        // return $entities;
    }

    private static function _adjustSuffixTypeEight(&$entities)
    {
        $endChar_na = array('ik'); //possibility to add more

        foreach ($entities as $entity) {
            $endChar = substr($entity->result, strlen($entity->result) - 1, 1);
            $endChar_2 = substr($entity->result, strlen($entity->result) - 2, 2);
            if (in_array($endChar_2, $endChar_na)) {
                $entity->result .= 'na';
                continue;
            }
            switch ($endChar) {
                case 'u'    :
                    $entity->result = substr($entity->result, 0, strlen($entity->result) - 1);
                    $entity->result  .= 'onanipun';
                    break;

                case 'a'    :
                case 'o'    :
                case 'e'    :
                    $entity->result  .= 'nanipun';
                    break;

                case 'i'    :
                    $entity->result = substr($entity->result, 0, -1);
                    $entity->result  .= 'enanipun';
                    break;
                default     :
                    $entity->result  .= 'anipun';
                    break;
            }
        }

        // return $entities;
    }
    //endregion suffix

    //region prefix
    private static function _adjustPrefixTypeOne(&$entities, $prefix)
    {
        foreach ($entities as $entity) {
            switch ($prefix) {
                case 'di':
                    $entity->result = 'dipun'.$entity->result;
                    break;

                case 'se':
                    $entity->result = 'sa'.$entity->result;
                    break;

                case 'ku':
                    $entity->result = self::Krama_Saya.' '.$entity->result;
                    break;

                case 'kau':
                    $entity->result = $entity->result_language_id == Constants::LanguageId('jv-KR')?  self::Krama_Kamu :  self::KramaInggil_Kamu;
                    $entity->result = ' '.$entity->result;
                    break;

                case 'menge':
                    $entity->result = 'nge'.$entity->result;
                    break;

                case 'penge':
                    $entity->result = 'ingkang '.'nge'.$entity->result;
                    break;

                default:
                    break;
            }
        }

        // return $entities;
    }

    private static function _adjustPrefixTypeThree(&$entities)
    {
        foreach ($entities as $entity) {
            $entity->result = 'pa'.$entity->result;
        }

        // return $entities;
    }

    private static function _adjustPrefixTypeFour(&$entities, $word_type_id)
    {
        if ($word_type_id == 'sifat') {
            var_dump('tbd');
            die;

            //return $this->cek_akhiran_kan($tembung_golek);
        }

        // return $entities;
    }

    private static function _adjustPrefixTypeFive(&$entities, $word_type_id)
    {
        switch ($word_type_id) {
            case Constants::WordTypes('Verb'):
            case Constants::WordTypes('Number'):

                //eg. berbohong
                    self::_adjustPrefixTypeTwo($entities);

                break;

            case Constants::WordTypes('Adjective'):
            case Constants::WordTypes('Pronoun'):
            case Constants::WordTypes('Noun'):

                //eg. bernilai
                foreach ($entities as $entity) {
                    $entity->result = 'gadhah '.$entity->result;
                }

                break;

            default:

                break;
        }

        // return $entities;
    }

    private static function _adjustPrefixTypeSeven(&$entities, $word_type_id)
    {
        switch ($word_type_id) {
            case Constants::WordTypes('Adjective'):

                //return $this->cek_akhiran_kan($tembung_golek);
                BaseDebugger::debug("Tobe develope");
                die;
                break;

            case Constants::WordTypes('Verb'):
            case Constants::WordTypes('Number'):
            case Constants::WordTypes('Pronoun'):
            case Constants::WordTypes('Noun'):

                foreach ($entities as $entity) {
                    $entity->result = 'saben '.$entity->result;
                }

                break;

            default:
                break;
        }

        // return $entities;
    }

    private static function _adjustPrefixTypeEight(&$entities, $word_type_id)
    {
        switch ($word_type_id) {
            case Constants::WordTypes('Adjective'):

                foreach ($entities as $entity) {
                    $entity->result = 'paling '.$entity->result;
                }

                break;

            case Constants::WordTypes('Verb'):

                self::_adjustPrefixJavaneseTypeSix($entities);

                break;

            case Constants::WordTypes('Number'):
            case Constants::WordTypes('Pronoun'):
            case Constants::WordTypes('Noun'):
            default:

                break;
        }

        // return $entities;
    }

    private static function _adjustPrefixTypeNine(&$entities)
    {
        foreach ($entities as $entity) {
            var_dump('tbd');
            die;

            // return "di".$this->cek_akhiran_kan($tembung_golek);
            $entity->result = 'di'.$entity->result;
        }

        // return $entities;
    }
}
