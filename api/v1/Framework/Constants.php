<?php

namespace Base\Framework;
use Base\Framework\DevTools\BaseDebugger;

class Constants
{
    public static $SystemID = 2;

    private static $messageTypes = array(
        'Success' => 'success',
        'Warning' => 'warning',
        'Error' => 'error',
        'Info' => 'info',
    );

    public static function getMessageType()
    {
        return self::$messageTypes;
    }

    private static $errorCodes = array(
        'BadRequest' => 400,
        'Unauthorized' => 401,
        'Forbidden' => 403,
        'NotFound' => 404,
        'InternalServerError' => 500,
    );

    public static function errorCode()
    {
        return self::$errorCodes;
    }

    public static $language = array(
        'id-ID' => array (1, 'id-ID'),
        'jv-NG' => array (2, 'jv-NG'),
        'jv-KR' => array (3, 'jv-KR'),
        'jv-KI' => array (4, 'jv-KI'),
        'en-US' => array (5, 'en-US'),
        'sd-SD' => array (6, 'sd-SD'),
        'sd-HL' => array (7, 'sd-HL'),
    );

    public static function LanguageId($code = null)
    {
        return $code ? self::$language[$code][0] : self::$language;
    }

    // public static $languageCode = array(
    //     'id-ID' => 'id-ID',
    //     'jv-NG' => 'jv-NG',
    //     'jv-KR' => 'jv-KR',
    // );

    public static function LanguageCode($code = null)
    {
        return $code ? self::$language[$code][1] : self::$language;
    }

    public static $particleCode = array(
        'Prefix' => 'prefix',
        'Suffix' => 'suffix',
    );

    public static function particleCode()
    {
        return self::$particleCode;
    }

    public static $wordTypes = array(
        'Verb' => 1,
        'Adjective' => 2,
        'Pronoun' => 3,
        'Noun' => 4,
        'Adverb' => 5,
        'Conjunction' => 6,
        'Number' => 7,
        'Question' => 8,
    );

    public static function WordTypes($key = null)
    {
        return $key ? self::$wordTypes[$key] : self::$wordTypes;
    }

    private static $particleTypes = array(
        'PrefixType1' => 'PrefixType1',
        'PrefixType2' => 'PrefixType2',
        'PrefixType3' => 'PrefixType3',
        'PrefixType4' => 'PrefixType4',
        'PrefixType5' => 'PrefixType5',
        'PrefixType6' => 'PrefixType6',
        'PrefixType7' => 'PrefixType7',
        'PrefixType8' => 'PrefixType8',
        'PrefixType9' => 'PrefixType9',
        'SuffixType1' => 'SuffixType1',
        'SuffixType2' => 'SuffixType2',
        'SuffixType3' => 'SuffixType3',
        'SuffixType4' => 'SuffixType4',
        'SuffixType5' => 'SuffixType5',
        'SuffixType6' => 'SuffixType6',
        'SuffixType7' => 'SuffixType7',
        'SuffixType8' => 'SuffixType8',
        'SuffixType9' => 'SuffixType9',
        'SuffixType10' => 'SuffixType10',
    );

    public static function ParticleType($id = null)
    {
        return $id ? self::$particleTypes[$id] : self::$particleTypes;
    }

    private static $prefixes = array(
        'id-ID' => array(
            'PrefixType1' => array('di', 'se', 'ku', 'kau', 'menge', 'penge'),
            'PrefixType2' => array('me', 'mem', 'men', 'meny', 'meng'),
            'PrefixType3' => array('pe', 'pen', 'pem', 'peng', 'peny'),
            'PrefixType4' => array('memper'), //case : memperdalam, memperbanyak, mempekerjakan, memperindah
            'PrefixType5' => array('ber'),
            'PrefixType6' => array('ke'),
            'PrefixType7' => array('per'),
            'PrefixType8' => array('ter'),
            'PrefixType9' => array('diper'),
            ),
        'jv-NG' => array(
            'PrefixType1' => array('di','kok','dak','ke','sa','sak','se'),
            'PrefixType2' => array('n','m','ng','ny'),
            'PrefixType3' => array('pa','pang','pam'),
            ),
        'jv-KR' => array(
            // 'PrefixType1' => array('di','menge'),
            // 'PrefixType2' => array('diper','memper','menge','meng','meny','mem','men','me','ber','di','per','penge','peng','pen','pem','pe','ter','se','ke', 'ku','kau','m'),
            // 'PrefixType3' => array('pa','pang','pam'),
            ),
    );

    public static function Prefix($languageId, $type = null)
    {
        if(!isset(self::$prefixes[$languageId])) return array();
        return $type ? self::$prefixes[$languageId][$type] : self::$prefixes[$languageId];
    }

    private static $suffixes = array(
        'id-ID' => array(
            'SuffixType1' => array('kan', 'in', 'kanmu', 'kanku', 'kannya'),
            'SuffixType2' => array('an'),
            'SuffixType3' => array('i', 'imu', 'iku'),
            'SuffixType4' => array('lah'),
            'SuffixType5' => array('mu', 'ku', 'anku', 'anmu'),
            'SuffixType6' => array('nya', 'annya'),
            'SuffixType7' => array('inya'),
            'SuffixType8' => array('ilah', 'ipun'),
            'SuffixType9' => array('pun'),
            'SuffixType10' => array('kanpun'),
            ),
        'jv-NG' => array(
            'SuffixType1' => array('e','ne','ake','ke','na','on','kake','ku','mu','okake','okne','okna','kne','kna','ekna','ekne','enake','ekake'),
            'SuffixType2' => array('a','i','nen','en','ni','a','oni','eni'),
            'SuffixType3' => array('n','an','nan'),
            'SuffixType4' => array('nana','onana','enana'),
            'SuffixType5' => array('nane','onane','enane'),
            ),
    );

    public static function Suffix($languageId, $type = null)
    {
        if(!isset(self::$suffixes[$languageId])) return array();
        return $type ? self::$suffixes[$languageId][$type] : self::$suffixes[$languageId];
    }

    private static $rules = array(
        'SwitchNext' => 1,
        'SwitchPrevious' => 2,
    );

    public static function Rule($id = null)
    {
        return $id ? self::$rules[$id] : self::$rules;
    }


    private static $statuses = array(
        'Active' => 1,
        'Inactive' => 2,
        'Pending' => 3,
        'Review' => 4,
        'Blocked' => 5,
        'Suggested' => 6
    );

    public static function Status($id = null)
    {
        return $id ? self::$statuses[$id] : self::$statuses;
    }
}
