<?php
namespace Base\Models;
use Phalcon\Mvc\Model;
class Particle extends BaseModel
{
    /**
     * [$particle description]
     * @var [type]
     */
    public $particle;

    /**
     * [$language_id description]
     * @var [type]
     */
    public $language_id;

    /**
     * [$description description]
     * @var [type]
     */
    public $description;

    /**
     * [$type description]
     * @var [type]
     */
    public $type;

    /**
     * [$status description]
     * @var [type]
     */
    public $status;
    
    /**
     * [$order description]
     * @var [type]
     */
    public $order;
    
    /**
     * This model is mapped to the table Particle
     */
    public function getSource() {
        return 'Particle';
    }
    
    /**
     * table relationship
     */
    public function initialize() {
        $this->belongsTo('language_id', 'Language', 'id');
        $this->belongsTo('status_id', 'Status', 'id');
    }
}
