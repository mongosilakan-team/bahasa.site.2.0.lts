<?php

namespace Base\Models;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class UserTranslations extends \Phalcon\Mvc\Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var int
     */
    public $translation_id;

    /**
     * @var string
     */
    // public $created_at;

    public function initialize()
    {
        $this->belongsTo('user_id', "Base\Models\User", 'id');
        $this->belongsTo('translation_id', "Base\Models\Translation", 'id');
    }

    public function getSource() {
        return 'user_translations';
    }

	public function beforeValidationOnCreate() {
        $this->created_at = gmdate('Y-m-d H:i:s');
    }

	public function validation()
    {
        $validator = new Validation();

        $validator->add(
           array('user_id', 'translation_id'),
           new UniquenessValidator([
               'message' => "User translations must be unique."
           ])
        );

        return $this->validationHasFailed() != true;
    }
}
