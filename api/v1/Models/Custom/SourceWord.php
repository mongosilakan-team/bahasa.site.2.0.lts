<?php
namespace Base\Models\Custom;

/**
 * SourceWord class
 */
class SourceWord
{
	public function __construct($word, $languageId, $length, $position, $isAlphabetic, $order)
    {
		$this->word = $word;
		$this->languageId = $languageId;
		$this->length = $length;
		$this->position = $position;
		$this->isAlphabetic = $isAlphabetic;
		$this->order = $order;
	}

	public $word;

	public $root;

	public $languageId;

	public $prefix;

	public $prefixType;

	public $suffix;

	public $suffixType;

	public $length;

	public $isAlphabetic;

    public $position = array();

	public $order;

}
