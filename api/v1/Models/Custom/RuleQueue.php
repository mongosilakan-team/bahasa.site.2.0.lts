<?php
namespace Base\Models\Custom;

/**
 * ViewTranslationResponse class
 */
class RuleQueue
{
	public function __construct() {
    }

	public static function init($id, $counter = 0) {
    	$instance = new self();
		$instance->counter = $counter;
		$instance->id = $id;
    	return $instance;
    }

	public $counter;

    public $id;

}
