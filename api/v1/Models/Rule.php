<?php
namespace Base\Models;
class Rule extends CacheableBaseModel
{

    /**
     *
     * @var string
     */
    public $name;

	public $description;

	public $status_id;

    public function initialize()
   {
       $this->belongsTo('status_id', 'Base\Models\Status', 'id',
           array(
               'reusable' => true
           ));

       $this->hasManyToMany(
           "id",
           "Base\Models\WordRules",
           "rule_id", "word_id",
           "Base\Models\Word",
           "id"
       );
   }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rules';
    }



    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Parts[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Parts
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
