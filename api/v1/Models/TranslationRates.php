<?php

namespace Base\Models;


use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class TranslationRates extends BaseModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $translation_id;

    /**
     * @var int
     */
	public $suggested_by;

    public $rates;

    public function initialize()
    {
        $this->belongsTo('translation_id', "Base\Models\Translation", 'id');
        $this->belongsTo('suggested_by', "Base\Models\User", 'id');
    }

    public function getSource() {
        return 'translation_rates';
    }

    /**
     * Allows to query a set of records that match the specified conditions.
     *
     * @param mixed $parameters
     *
     * @return RobotsParts[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions.
     *
     * @param mixed $parameters
     *
     * @return RobotsParts
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function validation() {
        $validator = new Validation();

        $validator->add(
           ['first_word_id', 'second_word_id', 'suggested_by'],
           new UniquenessValidator([
               'message' => "Translation rates must be unique.",
           ])
        );

        return $this->validate($validator);
    }

	// public function validation()
    // {
    //     $this->validate(
    //         new UniquenessValidator(
    //             array(
    //                 "field"   => array('translation_id', 'suggested_by'),
    //                 "message" => "Translation rates must be unique."
    //             )
    //         )
    //     );
    //
    //     return $this->validationHasFailed() != true;
    // }
}
