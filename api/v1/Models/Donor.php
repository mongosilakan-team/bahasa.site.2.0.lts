<?php

namespace Base\Models;

use Base\Framework\Messages\Message;
use Base\Framework\Constants;
use Base\Framework\Library\StringHelper;
use Base\Resources\Common\CommonResources;
use Base\Resources\Configuration\ConfigurationResources;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

/**
 * Donor class.
 */
class Donor extends BaseModel
{
    /**
     * name.
     *
     * @var string
     */
    public $name;

    /**
     * email.
     *
     * @var string
     */
    public $email;

    /**
     * phone.
     *
     * @var string
     */
    public $phone;

    /**
     * note.
     *
     * @var note
     */
    public $note;

    public function getSource()
    {
        return 'donors';
    }
    /**
     * A Donation only has a Donor, but a Donor have many Donantion.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Base\Models\Donation', 'donor_id', array(
            'foreignKey' => array(
                'message' => 'Donor cannot be deleted because it\'s used in Donation',
            ),
        ));
    }

    /**
     * @return bool
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
           'email', //your field name
            new EmailValidator([
               'model' => $this,
               'message' => StringHelper::format(CommonResources::getMessage('Msg_InvalidEmail'), ConfigurationResources::getMessage('email')),
            ])
       );

        $validator->add(
           'email',
            new UniquenessValidator([
               'model' => $this,
               'message' => StringHelper::format(CommonResources::getMessage('Msg_Uniqueness'), ConfigurationResources::getMessage('email'), $this->email),
            ])
       );

        return $this->validate($validator);
    }

    public function getMessages()
    {
        $messages = array();
        foreach (parent::getMessages() as $message) {
            switch ($message->getType()) {
                case 'PresenceOf':
                    $messages[] = new Message(null, Constants::getMessageType() ['Error'], StringHelper::format(CommonResources::getMessage('Msg_PresenceOf'), ConfigurationResources::getMessage($message->getField())));
                    break;

                default:
                    $messages[] = new Message(null, Constants::getMessageType() ['Error'], $message->getMessage());
                    break;
            }
        }

        return $messages;
    }
}
