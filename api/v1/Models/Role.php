<?php
namespace Base\Models;
class Role extends BaseModel
{

    /**
     *
     * @var string
     */
    public $name;

	public $description;

	public $status_id;

    public function initialize()
   {
	   $this->belongsTo('status_id', 'Base\Models\Status', 'id');
	   $this->hasManyToMany(
		   "id",
		   "Base\Models\RolePrivileges",
		   "role_id", "privilege_id",
		   "Base\Models\Privilege",
		   "id"
	   );
       $this->hasManyToMany(
           "id",
           "Base\Models\UserRoles",
           "role_id", "user_id",
           "Base\Models\User",
           "id"
       );
   }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'roles';
    }


    public function getPrivileges($parameters = null)
    {
         return  $this->getRelated('Base\Models\Privilege', $parameters);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Parts[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Parts
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
