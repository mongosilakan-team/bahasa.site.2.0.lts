<?php

namespace Base\Models;

use Phalcon\Mvc\Model;
use Base\Framework\Messages\Message;
use Base\Framework\Constants;
use Base\Framework\Library\StringHelper;
use Base\Resources\Common\CommonResources;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class Language extends CacheableBaseModel
{
    /**
     * language code.
     *
     * @var string
     */
    public $code;

    /**
     * [$name language].
     *
     * @var [type]
     */
    public $name;

    /**
     * Description.
     *
     * @var string
     */
    public $description;

    /**
     * Status identifier.
     *
     * @var int
     */
    public $status_id;

    /**
     * This model is mapped to the table language.
     */
    public function getSource()
    {
        return 'languages';
    }

    /**
     * A Word only has a Language, but a Language have many Words.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Base\Models\Word', 'language_id', array('foreignKey' => array('message' => 'Status cannot be deleted because it\'s used in other entity')));
        $this->belongsTo('status_id', 'Base\Models\Status', 'id');
    }

    /**
     * @return bool
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
           'code',
            new UniquenessValidator([
               'model' => $this,
               'message' => StringHelper::format(CommonResources::getMessage('Msg_Uniqueness'), CommonResources::getMessage('code'), $this->code),
            ])
        );

        return $this->validate($validator);
    }

    public function getMessages()
    {
        $messages = array();
        foreach (parent::getMessages() as $message) {
            switch ($message->getType()) {
                case 'PresenceOf':
                    $messages[] = new Message(null, Constants::getMessageType() ['Error'], StringHelper::format(CommonResources::getMessage('Msg_PresenceOf'), DictionaryResources::getMessage($message->getField())));
                    break;

                default:
                    $messages[] = new Message(null, Constants::getMessageType() ['Error'], $message->getMessage());
                    break;
            }
        }

        return $messages;
    }
}
