<?php

namespace Base\Models;

use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class UserRoles extends \Phalcon\Mvc\Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var int
     */
    public $role_id;

    /**
     * @var string
     */
    // public $created_at;

    public function initialize()
    {
        $this->belongsTo('user_id', "Base\Models\User", 'id');
        $this->belongsTo('role_id', "Base\Models\Role", 'id');
    }

    public function getSource() {
        return 'user_roles';
    }


    /**
     * Allows to query a set of records that match the specified conditions.
     *
     * @param mixed $parameters
     *
     * @return RobotsParts[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions.
     *
     * @param mixed $parameters
     *
     * @return RobotsParts
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
