<?php
namespace Base\Models;
use Phalcon\Mvc\Model;
class Translation extends BaseModel
{
    /**
     * first word identifier
     * @var Int
     */
    public $first_word_id;

    /**
     * second word identifier
     * @var Int
     */
    public $second_word_id;

    /**
     * point
     * @var Int
     */
    public $point;

    /**
     * Status identifier
     * @var Int
     */
    public $status_id;

    /**
     * This model is mapped to the table Translation
     */
    public function getSource() {
        return 'translations';
    }

    /**
     * table relationship
     */
    public function initialize() {
        $this->hasMany('id', 'Base\Models\TranslationRates', 'translation_id', array('foreignKey' => array('message' => 'Translation cannot be deleted because it\'s used in other entity')));
        $this->belongsTo('first_word_id', 'Base\Models\Word', 'id');
        $this->belongsTo('second_word_id', 'Base\Models\Word', 'id');
        $this->belongsTo('status_id', 'Base\Models\Status', 'id');

        $this->hasManyToMany(
            "id",
            "Base\Models\UserTranslations",
            "translation_id", "user_id",
            "Base\Models\User",
            "id"
        );
    }
}
