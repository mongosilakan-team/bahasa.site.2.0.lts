<?php

namespace Base\Models;

use Phalcon\Mvc\Model;
use Base\Framework\Messages\Message;
use Base\Framework\Constants;
use Base\Framework\Library\StringHelper;
use Base\Resources\Common\CommonResources;
use Base\Resources\Configuration\ConfigurationResources;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class Status extends BaseModel
{
    /**
     * Name.
     *
     * @var string
     */
    public $name;

    /**
     * Description.
     *
     * @var string
     */
    public $description;

    /**
     * This model is mapped to the table sample_cars.
     */
    public function getSource()
    {
        return 'statuses';
    }

    /**
     * table relationship.
     */
    public function initialize()
    {
        $this->hasMany('id', 'User', 'status_id', array('foreignKey' => array('message' => 'Status cannot be deleted because it\'s used in other entity')));
        $this->hasMany('id', 'Word', 'status_id', array('foreignKey' => array('message' => 'Status cannot be deleted because it\'s used in other entity')));
        $this->hasMany('id', 'TranslationLanguage', 'status_id', array('foreignKey' => array('message' => 'Status cannot be deleted because it\'s used in other entity')));
        $this->hasMany('id', 'Language', 'status_id', array('foreignKey' => array('message' => 'Status cannot be deleted because it\'s used in other entity')));
        $this->hasMany('id', 'Translation', 'status_id', array('foreignKey' => array('message' => 'Status cannot be deleted because it\'s used in other entity')));
    }

    /**
     * @return bool
     */
    // public function validation() {
    //     $this->validate(new UniquenessValidator(array('field' => 'name', 'message' => StringHelper::format(CommonResources::getMessage('Msg_Uniqueness'), ConfigurationResources::getMessage('status'), $this->name))));
    //     if ($this->validationHasFailed() == true) {
    //         return false;
    //     }
    // }

    public function validation()
    {
        $validator = new Validation();

        $validator->add(
           'name',
           new UniquenessValidator([
               'message' => StringHelper::format(CommonResources::getMessage('Msg_Uniqueness'), ConfigurationResources::getMessage('status'), $this->name),
           ])
        );

        return $this->validate($validator);
    }

    public function getMessages()
    {
        $messages = array();
        if (!parent::getMessages()) {
            return $messages;
        }
        foreach (parent::getMessages() as $message) {
            switch ($message->getType()) {
                case 'PresenceOf':
                    $messages[] = new Message(null, Constants::getMessageType() ['Error'], StringHelper::format(CommonResources::getMessage('Msg_PresenceOf'), ConfigurationResources::getMessage($message->getField())));
                    break;
                default:
                    $messages[] = new Message(null, Constants::getMessageType() ['Error'], $message->getMessage());
                    break;
            }
        }

        return $messages;
    }
}
