<?php

namespace Base\Controllers\Translate;

use Base\Services\TranslateService;
use Base\Services\TranslationRatesService;
use Base\Services\UserService;
use Base\Services\WordService;
use Base\Services\SuggestionService;
use Base\Framework\DevTools\BaseDebugger;
use Base\Models\Word;
use Base\Models\RobotsParts;
use Base\Models\Role;
use Base\Framework\Library\GibberishAES;

class TranslateController extends \Base\Controllers\ApiController
{
    /**
     * Service initialization.
     *
     * @var null
     */
    protected $translateService = null;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->translateService = new TranslateService();
        $this->wordService = new WordService();
        $this->userService = new UserService();
    }

    public function get()
    {
        $response = $this->translateService->getAll();

        return $response;
    }

    public function translate()
    {
        // $from = $this->request->get('from');
        //
        // $to = $this->request->get('to');
        //
        // $source = $this->request->get('source');
        $entity = $this->validateEntity($this->request->getPost());
        $response = $this->translateService->translate($entity['from'], $entity['to'], $entity['source']);

        return $this->sendRespond($response);
    }

    public function translateget()
    {
        // BaseDebugger::debug($test);die;
        // $word = $this->wordService->getById(1743);
        // BaseDebugger::debug($word->model->rules);die;
        // $user = $this->userService->getByUsernameOrEmail('soetedja@gmail.com');
        // BaseDebugger::debug($user->model->roles[0]->privileges[0]->route);die;

        // try {
            // $role = Role::findFirstById(1);
            // BaseDebugger::debug($role->privileges);die;
        // } catch (Exception $e) {
        //     BaseDebugger::debug($e);die;
        // }

        // try {
        //
        //
        //     // $robot = Word::findFirstById(85);
        //     $robot = $this->wordService->getById(85);
        //     // BaseDe    bugger::debug($robot->model->rule);
        //
        //     $robotsParts =  $robot->model->rule;
        //
        //     foreach ($robotsParts as $key => $value) {
        //         BaseDebugger::debug($value);
        //     }
        //     die;
        //
        // } catch (Exception $e) {
        //     // return $e;
        //     BaseDebugger::debug($e);die;
        //
        // }

        $from = $this->request->get('from');

        $to = $this->request->get('to');

        $source = $this->request->get('source');
        // GibberishAES::size(128);
        // BaseDebugger::debug(substr($source, 0, -1));
        // BaseDebugger::debug(GibberishAES::dec(substr($source, 0, -1), 'ultra-strong-password'));die;
        $response = $this->translateService->translate($from, $to, $source);

        return $this->sendRespond($response);
    }

    public function saveSuggestion()
    {
        // BaseDebugger::debug($this->request->getPost());die;
        $entity = $this->validateEntity($this->request->getPost());
        $response = SuggestionService::saveSuggestion($entity);

        return $this->sendRespond($response);
    }

    public function getWordDictionaryDetails()
    {
        // BaseDebugger::debug($this->request->getPost());die;
        $entity = $this->validateEntity($this->request->getPost());
        $response = TranslateService::getWordDictionaryDetails($entity['id']);

        return $this->sendRespond($response);
    }

    public function saveRate()
    {
        // BaseDebugger::debug($this->request->getPost());die;
        $entity = $this->validateEntity($this->request->getPost());
        $response = TranslationRatesService::saveUserRates($entity);

        return $this->sendRespond($response);
    }
}
