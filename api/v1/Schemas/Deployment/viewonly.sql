-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 02 Mei 2016 pada 08.43
-- Versi Server: 5.5.49-0ubuntu0.14.04.1
-- Versi PHP: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `bahasa.site`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `UnionTranslation`
--
CREATE TABLE IF NOT EXISTS `UnionTranslation` (
`translation_id` int(11) unsigned
,`user_id` int(11)
,`first_word_id` int(11) unsigned
,`second_word_id` int(11) unsigned
,`translation_status_id` tinyint(4) unsigned
,`point` int(11) unsigned
,`suggested_by` int(11) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `ViewTranslation`
--
CREATE TABLE IF NOT EXISTS `ViewTranslation` (
`source` varchar(50)
,`source_id` int(11) unsigned
,`source_language_id` tinyint(4) unsigned
,`translation_id` int(11) unsigned
,`result_id` int(11) unsigned
,`result` varchar(50)
,`result_language_id` tinyint(4) unsigned
,`translation_status_id` tinyint(4) unsigned
,`is_on_dictionary` tinyint(1)
,`word_type_id` tinyint(3) unsigned
,`point` int(11) unsigned
,`user_id` int(11)
,`suggested_by` int(11) unsigned
,`possible_combination` int(11)
,`rates` decimal(10,0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_donation`
--
CREATE TABLE IF NOT EXISTS `vw_donation` (
`id` int(11) unsigned
,`donor_id` int(11) unsigned
,`method` varchar(45)
,`amount` varchar(45)
,`description` text
,`created_at` datetime
,`modified_at` datetime
,`donor_name` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_word`
--
CREATE TABLE IF NOT EXISTS `vw_word` (
`id` int(11) unsigned
,`language_id` tinyint(4) unsigned
,`word` varchar(50)
,`status_id` tinyint(4) unsigned
,`is_on_dictionary` tinyint(1)
,`word_type_id` tinyint(3) unsigned
,`description` varchar(300)
,`created_at` datetime
,`modified_at` datetime
,`language` varchar(50)
,`language_code` varchar(10)
,`word_type` varchar(20)
);
-- --------------------------------------------------------

--
-- Struktur untuk view `UnionTranslation`
--
DROP TABLE IF EXISTS `UnionTranslation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `UnionTranslation` AS select `t1`.`id` AS `translation_id`,NULL AS `user_id`,`t1`.`first_word_id` AS `first_word_id`,`t1`.`second_word_id` AS `second_word_id`,`t1`.`status_id` AS `translation_status_id`,`t1`.`point` AS `point`,`t1`.`suggested_by` AS `suggested_by` from `translations` `t1` union select `t`.`id` AS `translation_id`,`ut`.`user_id` AS `user_id`,`t`.`first_word_id` AS `first_word_id`,`t`.`second_word_id` AS `second_word_id`,`t`.`status_id` AS `translation_status_id`,`t`.`point` AS `point`,`t`.`suggested_by` AS `suggested_by` from (`user_translations` `ut` join `translations` `t` on((`t`.`id` = `ut`.`translation_id`)));

-- --------------------------------------------------------

--
-- Struktur untuk view `ViewTranslation`
--
DROP TABLE IF EXISTS `ViewTranslation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ViewTranslation` AS select `w`.`word` AS `source`,`w`.`id` AS `source_id`,`w`.`language_id` AS `source_language_id`,`t`.`translation_id` AS `translation_id`,`j`.`id` AS `result_id`,`j`.`word` AS `result`,`j`.`language_id` AS `result_language_id`,`t`.`translation_status_id` AS `translation_status_id`,`j`.`is_on_dictionary` AS `is_on_dictionary`,`j`.`word_type_id` AS `word_type_id`,`t`.`point` AS `point`,`t`.`user_id` AS `user_id`,`t`.`suggested_by` AS `suggested_by`,`ISCOMBINATIONPOSSIBLE`(`w`.`id`) AS `possible_combination`,`GETTRANSLATIONRATES`(`t`.`translation_id`) AS `rates` from ((`UnionTranslation` `t` join `words` `j` on(((`j`.`id` = `t`.`first_word_id`) or (`j`.`id` = `t`.`second_word_id`)))) join `words` `w` on(((`w`.`id` = `t`.`first_word_id`) or (`w`.`id` = `t`.`second_word_id`)))) where ((`w`.`id` <> `j`.`id`) and (`w`.`language_id` <> `j`.`language_id`));

-- --------------------------------------------------------

--
-- Struktur untuk view `vw_donation`
--
DROP TABLE IF EXISTS `vw_donation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_donation` AS select `dn`.`id` AS `id`,`dn`.`donor_id` AS `donor_id`,`dn`.`method` AS `method`,`dn`.`amount` AS `amount`,`dn`.`description` AS `description`,`dn`.`created_at` AS `created_at`,`dn`.`modified_at` AS `modified_at`,`dr`.`name` AS `donor_name` from (`donations` `dn` join `donors` `dr` on((`dr`.`id` = `dn`.`donor_id`)));

-- --------------------------------------------------------

--
-- Struktur untuk view `vw_word`
--
DROP TABLE IF EXISTS `vw_word`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_word` AS select `w`.`id` AS `id`,`w`.`language_id` AS `language_id`,`w`.`word` AS `word`,`w`.`status_id` AS `status_id`,`w`.`is_on_dictionary` AS `is_on_dictionary`,`w`.`word_type_id` AS `word_type_id`,`w`.`description` AS `description`,`w`.`created_at` AS `created_at`,`w`.`modified_at` AS `modified_at`,`l`.`name` AS `language`,`l`.`code` AS `language_code`,`wt`.`name` AS `word_type` from ((`words` `w` join `languages` `l` on((`w`.`language_id` = `l`.`id`))) left join `word_types` `wt` on((`wt`.`id` = `w`.`word_type_id`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
