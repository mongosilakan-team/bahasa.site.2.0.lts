ALTER TABLE `translation_rates`
DROP FOREIGN KEY `fk_translation_id`;
ALTER TABLE `translation_rates`
CHANGE COLUMN `translation_id` `translation_id` INT(11) UNSIGNED NULL ,
ADD COLUMN `first_word_id` INT(11) UNSIGNED NULL AFTER `modified_at`,
ADD COLUMN `second_word_id` INT(11) UNSIGNED NULL AFTER `first_word_id`;
ALTER TABLE `translation_rates`
ADD CONSTRAINT `fk_translation_id`
  FOREIGN KEY (`translation_id`)
  REFERENCES translations` (`id`)
  ON UPDATE CASCADE;

ALTER TABLE `translation_rates`
DROP INDEX `unique_id` ,
ADD UNIQUE INDEX `unique_id` (`suggested_by` ASC, `first_word_id` ASC, `second_word_id` ASC);


//////////////////
DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `GETINDONESIANIDS`(`id` INT) RETURNS varchar(100) CHARSET latin1
BEGIN
DECLARE ids varchar(100);
	 set ids =(select group_concat(distinct(ts.first_word_id))
		from translations ts join words wts on ts.first_word_id = wts.id join words swts on ts.second_word_id = swts.id
		where ts.first_word_id = wts.id and swts.id = id and wts.language_id = 1);

RETURN ids;
END$$
DELIMITER ;
/////////////////////
DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STR`(
x VARCHAR(255),
delim VARCHAR(12),
pos INT
) RETURNS varchar(255) CHARSET latin1
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
     LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
     delim, '')$$
DELIMITER ;
/////////////////////////
USE `klmlzmorfvbgb`;
CREATE
     OR REPLACE ALGORITHM = UNDEFINED
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `ViewTranslation` AS
    select
        GETINDONESIANIDS(`w`.`id`) AS `idn_ids`,
        `w`.`word` AS `source`,
        `w`.`id` AS `source_id`,
        `w`.`language_id` AS `source_language_id`,
        `w`.`word_root_id` AS `source_root_id`,
        `w`.`prefix` AS `source_prefix`,
        `w`.`suffix` AS `source_suffix`,
        ((`w`.`prefix` is not null)
            or (`w`.`suffix` is not null)) AS `is_source_has_particle`,
        `j`.`word_root_id` AS `result_root_id`,
        `j`.`status_id` AS `result_status_id`,
        `j`.`prefix` AS `result_prefix`,
        `j`.`suffix` AS `result_suffix`,
        ((`j`.`prefix` is not null)
            or (`j`.`suffix` is not null)) AS `is_result_has_particle`,
        `t`.`translation_id` AS `translation_id`,
        `j`.`id` AS `result_id`,
        `j`.`word` AS `result`,
        `j`.`language_id` AS `result_language_id`,
        `t`.`translation_status_id` AS `translation_status_id`,
        `j`.`is_on_dictionary` AS `is_on_dictionary`,
        `j`.`word_type_id` AS `word_type_id`,
        `t`.`point` AS `point`,
        `t`.`user_id` AS `user_id`,
        `t`.`suggested_by` AS `suggested_by`,
        POSSIBLECOMBINATION(`w`.`id`) AS `possible_combination`,
        GETTRANSLATIONRATES(
                `t`.`first_word_id`,
                `t`.`second_word_id`) AS `rates`,
        NUMBEROFRULES(`w`.`id`) AS `number_of_rules`
    from
        ((`UnionTranslation` `t`
        join `words` `j` ON (((`j`.`id` = `t`.`first_word_id`)
            or (`j`.`id` = `t`.`second_word_id`))))
        join `words` `w` ON (((`w`.`id` = `t`.`first_word_id`)
            or (`w`.`id` = `t`.`second_word_id`))))
    where
        ((`w`.`id` <> `j`.`id`)
            and (`w`.`language_id` <> `j`.`language_id`));


///////////////////////////////////////////////////
DELIMITER $$

CREATE DEFINER=`root`@`localhost` FUNCTION `GETTRANSLATIONRATES`(fwi int, swi int) RETURNS decimal(10,0)
BEGIN
DECLARE rates decimal;

set rates = (select
                avg(`tr`.`rates`)
            from
                `translation_rates` tr
            where
                (((tr.first_word_id = fwi and tr.second_word_id = swi) or (tr.first_word_id = swi and tr.second_word_id = fwi))
				)) ;
RETURN rates;
END
/////////////////////////////////////
USE `klmlzmorfvbgb`;
DROP procedure IF EXISTS `SPViewTranslation`;

DELIMITER $$
USE `klmlzmorfvbgb`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SPViewTranslation`(typeGet int, wSource varchar(255), fromLanguage int, toLanguage int, user int)
BEGIN
	DECLARE a INT Default 0 ;
	DECLARE str VARCHAR(255);
	set user = IFNULL(user,0);
	create temporary table  IF NOT EXISTS tmpVt engine=memory select
	t.source_id as idn_id,
	t.source as idn_word
	, t.*
	from ViewTranslation t limit 0;

	case typeGet
		when 1 -- getSingleWord
		then
		set @idnIds = 0;
		SET @sss = wSource;
		insert into tmpVt (
			select @sId := vt.source_id, @idnIds := idn_ids, vt.*
			from ViewTranslation vt where vt.source = wSource
			and  vt.source_language_id = fromLanguage
			and (vt.translation_status_id = 1 or (vt.user_id = user and vt.translation_status_id = 6))
		);
		set @idnIds = IFNULL(@idnIds,0);
		set @sId = IFNULL(@sId,0);
		set @query = concat('insert into tmpVt ( select  vt.source_id, vt.source, vt.idn_ids, ''',wSource,''', ',@sId,', ',fromLanguage,', vt.source_root_id, vt.source_prefix, vt.source_suffix, vt.is_source_has_particle, vt.result_root_id, vt.result_status_id, vt.result_prefix, vt.result_suffix, vt.is_result_has_particle, vt.translation_id, vt.result_id, vt.result, vt.result_language_id, vt.translation_status_id, vt.is_on_dictionary, vt.word_type_id, vt.point, vt.user_id, vt.suggested_by, vt.possible_combination, GETTRANSLATIONRATES(',@sId,', vt.result_id) as rates, vt.number_of_rules from ViewTranslation vt where vt.source_id in (',@idnIds,') and vt.result_language_id = ',toLanguage,' and (vt.translation_status_id = 1 or (vt.user_id = ',user,' and vt.translation_status_id = 6)));');
		prepare sql_query from @query;
		execute sql_query;
		-- select @idnIds;
		-- insert into tmpVt (
		-- 	select vt.source, vt.idn_ids, source, vt.source_id, fromLanguage, vt.source_root_id, vt.source_prefix, vt.source_suffix, vt.is_source_has_particle, vt.result_root_id, vt.result_status_id, vt.result_prefix, vt.result_suffix, vt.is_result_has_particle, vt.translation_id, vt.result_id, vt.result, vt.result_language_id, vt.translation_status_id, vt.is_on_dictionary, vt.word_type_id, vt.point, vt.user_id, vt.suggested_by, vt.possible_combination, vt.translation_id, vt.number_of_rules
		-- from ViewTranslation vt where find_in_set(vt.source_id, @idnIds)
		-- and vt.result_language_id = toLanguage and (vt.translation_status_id = 1 or (vt.user_id = user and vt.translation_status_id = 6))
		-- and (vt.translation_status_id = 1 or (vt.user_id = user and vt.translation_status_id = 6))
		-- );

		when 2 -- getBulkWords
		then
			  simple_loop: LOOP
				set @idnIds = null;
				 SET a=a+1;
				 SET str=SPLIT_STR(wSource,",",a);
				 IF str='' THEN
					LEAVE simple_loop;
				 END IF;
				insert into tmpVt (
					select @sId := vt.source_id, @idnIds := idn_ids, vt.*
					from ViewTranslation vt where vt.source = str
					and  vt.source_language_id = fromLanguage
					and (vt.translation_status_id = 1 or (vt.user_id = user and vt.translation_status_id = 6))
				);
				set @idnIds = IFNULL(@idnIds,0);
				set @sId = IFNULL(@sId,0);
				set @query = concat('insert into tmpVt ( select  vt.source_id, vt.source, vt.idn_ids, ''',str,''', ',@sId,', ',fromLanguage,', vt.source_root_id, vt.source_prefix, vt.source_suffix, vt.is_source_has_particle, vt.result_root_id, vt.result_status_id, vt.result_prefix, vt.result_suffix, vt.is_result_has_particle, vt.translation_id, vt.result_id, vt.result, vt.result_language_id, vt.translation_status_id, vt.is_on_dictionary, vt.word_type_id, vt.point, vt.user_id, vt.suggested_by, vt.possible_combination, GETTRANSLATIONRATES(',@sId,', vt.result_id) as rates, vt.number_of_rules from ViewTranslation vt where vt.source_id in (',@idnIds,') and vt.result_language_id = ',toLanguage,' and (vt.translation_status_id = 1 or (vt.user_id = ',user,' and vt.translation_status_id = 6)));');
				prepare sql_query from @query;
				execute sql_query;

		   END LOOP simple_loop;
	end case;


	select tmpVt.* from tmpVt
	where source_language_id = fromLanguage and result_language_id = toLanguage
	order by rates desc;

	DROP temporary table tmpVt;

END$$

DELIMITER ;
