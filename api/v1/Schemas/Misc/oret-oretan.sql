select fw.word as first_word, sw.word as second_word, fl.name as first_language, sl.name as second_language, fwl.name as first_word_language from translation t
Inner join translation_language tl on t.translation_language_id = tl.id
inner join language fl on tl.first_language_id = fl.id
inner join language sl on tl.second_language_id = sl.id
inner join language fwl on tl.first_word_language_id = fwl.id
inner join word fw on t.first_word_id = fw.id
inner join word sw on t.second_word_id = sw.id

-- select * from translation t
-- inner join translation_language tl on tl.id = t.translation_language_id
-- where tl.first_language_id = 2  && tl.second_language_id = 1



select * from translation_language tl
where tl.first_language_id = 2  && tl.second_language_id = 1
or tl.first_language_id = 1  && tl.second_language_id = 2 limit 1;

select * from translation t 
where t.translation_language_id = 1 && t.second_word_id = 4163


////

select * from translation t 
inner join translation_language tl on t.translation_language_id = tl.id
inner join word fw on t.first_word_id = fw.id
inner join word sw on t.second_word_id = sw.id
where (first_word_id in
(select w.id from word  w where w.word = 'ana') 
or second_word_id in
(select w.id from word  w where w.word = 'ana'))
and t.translation_language_id = 
(select id from translation_language tl
where tl.first_language_id = 2  && tl.second_language_id = 1
or tl.first_language_id = 1  && tl.second_language_id = 2 limit 1)



select * from (
SELECT vbt.translation_language_id, vbt.first_word as indonesia, vbt.second_word as krama FROM vw_base_translation vbt
where vbt.translation_language_id = 2) as indo_krama;

select * from (
SELECT vbt.translation_language_id, vbt.first_word as indonesia, vbt.second_word as krama_inggil FROM vw_base_translation vbt
where vbt.translation_language_id = 3) as indo_kramainggil;

select distinct(first_word_id), second_word_id, translation_language_id from translation


//view translation management
select ID_NG.indonesia_id, ID_NG.indonesia, ID_NG.ngoko_id, ID_NG.ngoko, ID_KR.krama_id, ID_KR.krama,ID_KI.krama_inggil_id, ID_KI.krama_inggil  from (
SELECT vbt.translation_language_id,vbt.first_word_id as indonesia_id, vbt.first_word as indonesia, group_concat(vbt.second_word_id order by point desc SEPARATOR ', ') as ngoko_id, group_concat(vbt.second_word order by point desc SEPARATOR ', ') as ngoko FROM vw_base_translation vbt
where vbt.translation_language_id = 1 group by vbt.first_word_id) as ID_NG
inner join (select * from (
SELECT vbt.translation_language_id,vbt.first_word_id as indonesia_id, vbt.first_word as indonesia, group_concat(vbt.second_word_id order by point desc SEPARATOR ', ') as krama_id, group_concat(vbt.second_word order by point desc SEPARATOR ', ') as krama FROM vw_base_translation vbt
where vbt.translation_language_id = 2 group by vbt.first_word_id) as indo_krama) as ID_KR on ID_KR.indonesia_id = ID_NG.indonesia_id
inner join (select * from (
SELECT vbt.translation_language_id, vbt.first_word_id as indonesia_id, vbt.first_word as indonesia, group_concat(vbt.second_word_id order by point desc SEPARATOR ', ') as krama_inggil_id, group_concat(vbt.second_word SEPARATOR ', ') as krama_inggil FROM vw_base_translation vbt
where vbt.translation_language_id = 3 group by vbt.first_word_id) as indo_kramainggil) as ID_KI on ID_KI.indonesia_id = ID_NG.indonesia_id
 WHERE ID_NG.indonesia = 'adik'