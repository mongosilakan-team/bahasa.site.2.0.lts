CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `vw_base_translation` AS
    SELECT 
        `tl`.`id` AS `translation_language_id`,
        `t`.`first_word_id` AS `first_word_id`,
        `t`.`second_word_id` AS `second_word_id`,
        `t`.`point` AS `point`,
        `t`.`status_id` AS `status_id`,
        `tl`.`first_language_id` AS `first_language_id`,
        `tl`.`second_language_id` AS `second_language_id`,
        `tl`.`first_word_language_id` AS `first_word_language_id`,
        `fw`.`word` AS `first_word`,
        `sw`.`word` AS `second_word`
    FROM
        (((`translation` `t`
        JOIN `translation_language` `tl` ON ((`t`.`translation_language_id` = `tl`.`id`)))
        JOIN `word` `fw` ON ((`t`.`first_word_id` = `fw`.`id`)))
        JOIN `word` `sw` ON ((`t`.`second_word_id` = `sw`.`id`)))