-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: base.framework
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `vw_base_translation`
--

DROP TABLE IF EXISTS `vw_base_translation`;
/*!50001 DROP VIEW IF EXISTS `vw_base_translation`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_base_translation` AS SELECT 
 1 AS `translation_language_id`,
 1 AS `first_word_id`,
 1 AS `second_word_id`,
 1 AS `point`,
 1 AS `status_id`,
 1 AS `first_language_id`,
 1 AS `second_language_id`,
 1 AS `first_word_language_id`,
 1 AS `first_word`,
 1 AS `second_word`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_donation`
--

DROP TABLE IF EXISTS `vw_donation`;
/*!50001 DROP VIEW IF EXISTS `vw_donation`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_donation` AS SELECT 
 1 AS `id`,
 1 AS `donor_id`,
 1 AS `method`,
 1 AS `amount`,
 1 AS `description`,
 1 AS `created_at`,
 1 AS `modified_at`,
 1 AS `donor_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_manage_translation`
--

DROP TABLE IF EXISTS `vw_manage_translation`;
/*!50001 DROP VIEW IF EXISTS `vw_manage_translation`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_manage_translation` AS SELECT 
 1 AS `indonesian_id`,
 1 AS `indonesian`,
 1 AS `ngoko_ids`,
 1 AS `ngoko`,
 1 AS `indo_ngoko_points`,
 1 AS `krama_ids`,
 1 AS `krama`,
 1 AS `indo_krama_points`,
 1 AS `kramainggil_ids`,
 1 AS `kramainggil`,
 1 AS `indo_kramainggil_points`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_sub_query_indo_krama`
--

DROP TABLE IF EXISTS `vw_sub_query_indo_krama`;
/*!50001 DROP VIEW IF EXISTS `vw_sub_query_indo_krama`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_sub_query_indo_krama` AS SELECT 
 1 AS `translation_language_id`,
 1 AS `indonesia_id`,
 1 AS `indonesia`,
 1 AS `krama_id`,
 1 AS `krama`,
 1 AS `id_kr_point`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_sub_query_indo_kramainggil`
--

DROP TABLE IF EXISTS `vw_sub_query_indo_kramainggil`;
/*!50001 DROP VIEW IF EXISTS `vw_sub_query_indo_kramainggil`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_sub_query_indo_kramainggil` AS SELECT 
 1 AS `translation_language_id`,
 1 AS `indonesia_id`,
 1 AS `indonesia`,
 1 AS `krama_inggil_id`,
 1 AS `krama_inggil`,
 1 AS `id_ki_point`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_sub_query_indo_ngoko`
--

DROP TABLE IF EXISTS `vw_sub_query_indo_ngoko`;
/*!50001 DROP VIEW IF EXISTS `vw_sub_query_indo_ngoko`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_sub_query_indo_ngoko` AS SELECT 
 1 AS `translation_language_id`,
 1 AS `indonesia_id`,
 1 AS `indonesia`,
 1 AS `ngoko_id`,
 1 AS `ngoko`,
 1 AS `id_ng_point`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_word`
--

DROP TABLE IF EXISTS `vw_word`;
/*!50001 DROP VIEW IF EXISTS `vw_word`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_word` AS SELECT 
 1 AS `id`,
 1 AS `language_id`,
 1 AS `word`,
 1 AS `status_id`,
 1 AS `is_on_dictionary`,
 1 AS `word_type_id`,
 1 AS `description`,
 1 AS `created_at`,
 1 AS `modified_at`,
 1 AS `language`,
 1 AS `language_code`,
 1 AS `word_type`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vw_base_translation`
--

/*!50001 DROP VIEW IF EXISTS `vw_base_translation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_base_translation` AS select `tl`.`id` AS `translation_language_id`,`t`.`first_word_id` AS `first_word_id`,`t`.`second_word_id` AS `second_word_id`,`t`.`point` AS `point`,`t`.`status_id` AS `status_id`,`tl`.`first_language_id` AS `first_language_id`,`tl`.`second_language_id` AS `second_language_id`,`tl`.`first_word_language_id` AS `first_word_language_id`,`fw`.`word` AS `first_word`,`sw`.`word` AS `second_word` from (((`translation` `t` join `translation_language` `tl` on((`t`.`translation_language_id` = `tl`.`id`))) join `word` `fw` on((`t`.`first_word_id` = `fw`.`id`))) join `word` `sw` on((`t`.`second_word_id` = `sw`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_donation`
--

/*!50001 DROP VIEW IF EXISTS `vw_donation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_donation` AS select `dn`.`id` AS `id`,`dn`.`donor_id` AS `donor_id`,`dn`.`method` AS `method`,`dn`.`amount` AS `amount`,`dn`.`description` AS `description`,`dn`.`created_at` AS `created_at`,`dn`.`modified_at` AS `modified_at`,`dr`.`name` AS `donor_name` from (`donation` `dn` join `donor` `dr` on((`dr`.`id` = `dn`.`donor_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_manage_translation`
--

/*!50001 DROP VIEW IF EXISTS `vw_manage_translation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_manage_translation` AS select `id_ng`.`indonesia_id` AS `indonesian_id`,`id_ng`.`indonesia` AS `indonesian`,`id_ng`.`ngoko_id` AS `ngoko_ids`,`id_ng`.`ngoko` AS `ngoko`,`id_ng`.`id_ng_point` AS `indo_ngoko_points`,`id_kr`.`krama_id` AS `krama_ids`,`id_kr`.`krama` AS `krama`,`id_kr`.`id_kr_point` AS `indo_krama_points`,`id_ki`.`krama_inggil_id` AS `kramainggil_ids`,`id_ki`.`krama_inggil` AS `kramainggil`,`id_ki`.`id_ki_point` AS `indo_kramainggil_points` from ((`vw_sub_query_indo_ngoko` `id_ng` join `vw_sub_query_indo_krama` `id_kr` on((`id_kr`.`indonesia_id` = `id_ng`.`indonesia_id`))) join `vw_sub_query_indo_kramainggil` `id_ki` on((`id_ki`.`indonesia_id` = `id_ng`.`indonesia_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_sub_query_indo_krama`
--

/*!50001 DROP VIEW IF EXISTS `vw_sub_query_indo_krama`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_sub_query_indo_krama` AS select `vbt`.`translation_language_id` AS `translation_language_id`,`vbt`.`first_word_id` AS `indonesia_id`,`vbt`.`first_word` AS `indonesia`,group_concat(`vbt`.`second_word_id` order by `vbt`.`point` DESC separator ', ') AS `krama_id`,group_concat(`vbt`.`second_word` order by `vbt`.`point` DESC separator ', ') AS `krama`,group_concat(`vbt`.`point` order by `vbt`.`point` DESC separator ', ') AS `id_kr_point` from `vw_base_translation` `vbt` where (`vbt`.`translation_language_id` = 2) group by `vbt`.`first_word_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_sub_query_indo_kramainggil`
--

/*!50001 DROP VIEW IF EXISTS `vw_sub_query_indo_kramainggil`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_sub_query_indo_kramainggil` AS select `vbt`.`translation_language_id` AS `translation_language_id`,`vbt`.`first_word_id` AS `indonesia_id`,`vbt`.`first_word` AS `indonesia`,group_concat(`vbt`.`second_word_id` order by `vbt`.`point` DESC separator ', ') AS `krama_inggil_id`,group_concat(`vbt`.`second_word` order by `vbt`.`point` ASC separator ', ') AS `krama_inggil`,group_concat(`vbt`.`point` order by `vbt`.`point` DESC separator ', ') AS `id_ki_point` from `vw_base_translation` `vbt` where (`vbt`.`translation_language_id` = 3) group by `vbt`.`first_word_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_sub_query_indo_ngoko`
--

/*!50001 DROP VIEW IF EXISTS `vw_sub_query_indo_ngoko`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_sub_query_indo_ngoko` AS select `vbt`.`translation_language_id` AS `translation_language_id`,`vbt`.`first_word_id` AS `indonesia_id`,`vbt`.`first_word` AS `indonesia`,group_concat(`vbt`.`second_word_id` order by `vbt`.`point` DESC separator ', ') AS `ngoko_id`,group_concat(`vbt`.`second_word` order by `vbt`.`point` DESC separator ', ') AS `ngoko`,group_concat(`vbt`.`point` order by `vbt`.`point` DESC separator ', ') AS `id_ng_point` from `vw_base_translation` `vbt` where (`vbt`.`translation_language_id` = 1) group by `vbt`.`first_word_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_word`
--

/*!50001 DROP VIEW IF EXISTS `vw_word`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_word` AS select `w`.`id` AS `id`,`w`.`language_id` AS `language_id`,`w`.`word` AS `word`,`w`.`status_id` AS `status_id`,`w`.`is_on_dictionary` AS `is_on_dictionary`,`w`.`word_type_id` AS `word_type_id`,`w`.`description` AS `description`,`w`.`created_at` AS `created_at`,`w`.`modified_at` AS `modified_at`,`l`.`name` AS `language`,`l`.`code` AS `language_code`,`wt`.`name` AS `word_type` from ((`word` `w` join `language` `l` on((`w`.`language_id` = `l`.`id`))) left join `word_type` `wt` on((`wt`.`id` = `w`.`word_type_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-27 17:46:00
