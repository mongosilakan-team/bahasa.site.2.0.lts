
select w.word as source,
	w.id as source_id,
`w`.`language_id` AS `source_language_id`,
        `w`.`word_root_id` AS `source_root_id`,
        `w`.`prefix` AS `source_prefix`,
        `w`.`suffix` AS `source_suffix`,
        ((`w`.`prefix` is not null)
            or (`w`.`suffix` is not null)) AS `is_source_has_particle`,
 `j`.`word_root_id` AS `result_root_id`,
        `j`.`prefix` AS `result_prefix`,
        `j`.`suffix` AS `result_suffix`,
        ((`j`.`prefix` is not null)
            or (`j`.`suffix` is not null)) AS `is_result_has_particle`,
        `t`.`translation_id` AS `translation_id`,
        `j`.`id` AS `result_id`,
        `j`.`word` AS `result`,
        `j`.`language_id` AS `result_language_id`,
        `t`.`translation_status_id` AS `translation_status_id`,
        `j`.`is_on_dictionary` AS `is_on_dictionary`,
        `j`.`word_type_id` AS `word_type_id`,
        `t`.`point` AS `point`,
        `t`.`user_id` AS `user_id`,
        `t`.`suggested_by` AS `suggested_by`,
		(select count(v.id) from words w0 join words v where w0.language_id = v.language_id  and w0.word <> v.word and w0.word like concat(v.word,'%') and v.id = w.id  GROUP BY `v`.`id`) as possible_combination,
		(SELECT avg(rates) from translation_rates where translation_id = t.translation_id) as rates,
		(SELECT count(w1.id) from words w1 join word_rules wr on w1.id = wr.word_id where w1.id = w.id) as number_of_rules
from UnionTranslation t
join words j on j.id = t.first_word_id or j.id = t.second_word_id
join words w on w.id = t.first_word_id or w.id = t.second_word_id
where j.id = (select w.id as key_id from UnionTranslation t
join words j on j.id = t.first_word_id or j.id = t.second_word_id
join words w on w.id = t.first_word_id or w.id = t.second_word_id
where j.word = w.word
and j.language_id = 6
and w.language_id = 1
and user_id is null)
and j.language_id = 1 and w.language_id = 2
and user_id is null;
