<?php
namespace Base\Services;
use Base\Framework\Constants;
use Base\Framework\DevTools\BaseDebugger;
use Base\Framework\Exceptions\CustomException;
use Base\Framework\Library\StringHelper;
use Base\Framework\Messages\Message;
use Base\Framework\Responses\Response;
use Base\Repositories\UserRepository;
use Base\Resources\Common\CommonResources;
use Base\Services\Interfaces\IUserService;

/**
 * User service
 */
class UserService implements IUserService
{

    /**
     * @param  string
     * @return the user
     */
    public static function getByUsernameOrEmail($userOrEmail) {
        $response = new Response();
        $user = UserRepository::getByUsernameOrEmail($userOrEmail);
        if ($user) {
            $response->model = $user;
        }
        else {
            $response->messages[] = new Message(null, Constants::getMessageType() ['Error'], StringHelper::format(CommonResources::getMessage('NotFound'), CommonResources::getMessage('User')));
        }
        return $response;
    }

    /**
     * @return the collection of user
     */
    public function getAll() {
        $response = new Response();
        $response->model = UserRepository::getAll();
        return $response;
    }

    /**
     * @param  criteria
     * @return the collection of user
     */
    public function search($criteria) {
        $response = new Response();
        $repository = new UserRepository();
        $response->model = $repository->search($criteria);
        return $response;
    }

    /**
     * @param  identifier
     * @return the user
     */
    public static function getById($id) {
        $response = new Response();
        $result = UserRepository::getById($id);
        if ($result) {
            $response->model = $result;
            return $response;
        }
        else {
            $response->messages[] = new Message(null, Constants::getMessageType() ['Error'], StringHelper::format(CommonResources::getMessage('NotFound'), CommonResources::getMessage('User')));
            throw new CustomException($response->messages, Constants::errorCode() ['NotFound']);
        }
    }

    /**
     * @param  entity
     * @return the messages
     */
    public static function create($entity) {
        $response = new Response();
        $model = UserRepository::create($entity);
        $savedUser = UserRepository::setDefaultRole($model->id);
        if ($model->getMessages()) {
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);
        }
        else {
            $response->model = $model;
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], StringHelper::format(CommonResources::getMessage('Msg_SuccessfullyCreated'), CommonResources::getMessage('User'), $entity['name']));
        }
        return $response;
    }

    /**
     * @param  entity
     * @return the messages
     */
    public static function update($entity) {
        $response = new Response();
        $entity['skip_attributes'] = array('password', 'created_at');
        $errMsg = UserRepository::update($entity);
        if ($errMsg) {
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);
        }
        else {
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], StringHelper::format(CommonResources::getMessage('Msg_SuccessfullyUpdated'), CommonResources::getMessage('User'), $entity['name']));
        }
        return $response;
    }

    /**
     * @param  entity
     * @return the messages
     */
    public static function updateFromModel($model) {
        $response = new Response();
        $errMsg = UserRepository::updateFromModel($model);
        if ($errMsg) {
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);
        }
        else {
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], StringHelper::format(CommonResources::getMessage('Msg_SuccessfullyUpdated'), CommonResources::getMessage('User'), $model->name));
        }
        return $response;
    }

    /**
     * @param  identfier
     * @return the messages
     */
    public function delete($id) {
        $response = new Response();
        $entity = UserRepository::getById($id);
        $errMsg = UserRepository::delete($id);
        if ($errMsg) {
            throw new CustomException($errMsg, Constants::errorCode() ['BadRequest']);
        }
        else {
            $response->messages[] = new Message(null, Constants::getMessageType() ['Success'], StringHelper::format(CommonResources::getMessage('Msg_SuccessfullyDeleted'), CommonResources::getMessage('User'), $entity->name));
        }

        return $response;
    }
}
