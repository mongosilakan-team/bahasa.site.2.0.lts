<?php
namespace Base\Services\Interfaces;

interface IParticleService extends IBaseService
{
    public function getByTypeAndLanguageId($type, $language_id);
}
