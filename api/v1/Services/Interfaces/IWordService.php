<?php
namespace Base\Services\Interfaces;

interface IWordService extends IBaseService
{
	public static function getByWordAndLanguageCode($word, $languageCode);
	public static function getByWordAndLanguageId($word, $languageId);
	public static function getSuggestionWordDictionary($word, $languageId);
}
