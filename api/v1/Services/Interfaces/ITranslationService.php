<?php
namespace Base\Services\Interfaces;

interface ITranslationService extends IBaseService
{
	public static function checkExistingTranslation($firstWordId, $secondWordId);
}
