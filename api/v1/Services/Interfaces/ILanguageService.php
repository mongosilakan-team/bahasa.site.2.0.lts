<?php
namespace Base\Services\Interfaces;

interface ILanguageService extends IBaseService
{
    public static function getByCode($code);
    public static function getActive();
    public static function getActiveAndPending();
    public static function getUserLanguages();
}
