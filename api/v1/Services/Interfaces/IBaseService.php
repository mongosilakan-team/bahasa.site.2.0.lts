<?php
namespace Base\Services\Interfaces;

interface IBaseService {
    public static function create($entity);
    public function delete($entity);
    public function getAll();
    public static function getById($id);
    public function search($criteria);
    public static function update($entity);
}
