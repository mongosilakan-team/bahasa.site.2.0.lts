<?php
namespace Base\Services\Interfaces;

interface IUserService extends IBaseService{
    public static function getByUsernameOrEmail($userOrEmail);
    public static function updateFromModel($model);
}
