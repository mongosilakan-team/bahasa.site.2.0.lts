<?php

namespace Base\Repositories;

use Base\Models\Role;
use Base\Models\User;
use Base\Repositories\Interfaces\IUserRepository;
use Base\Framework\DevTools\BaseDebugger;

class UserRepository extends BaseRepository implements IUserRepository
{
    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Define used model.
     */
    public static function Model()
    {
        return new User();
    }

    public function getViewModelName()
    {
        return '\Base\Models\User';
    }

    ### Additional method

    /**
     *
     */
    public static function getByUsernameOrEmail($userOrEmail)
    {
        return User::findFirst(array("(username = '$userOrEmail' OR email = '$userOrEmail')"));
    }

    public static function setDefaultRole($userId)
    {
        try {
            $user = User::findFirstById($userId);
            $roles = array();
            $role = Role::findFirstById(4);// Common user
            $roles[] = $role;
            $user->roles = $roles;

            return $user->save();

        } catch (Phalcon\Mvc\Model\Transaction\Failed $e) {
            throw new CustomException($model->getMessages(), Constants::errorCode() ['InternalServerError']);
        } catch (\Exception $e) {
            if (!empty($model->getMessages())) {
                BaseDebugger::debug($model->getMessages());
                die;
                throw new CustomException($model->getMessages(), Constants::errorCode() ['InternalServerError']);
            } else {
                var_dump($e->getMessage());
                throw $e;
            }
        }
    }

}
