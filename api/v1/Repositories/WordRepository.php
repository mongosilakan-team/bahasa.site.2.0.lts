<?php

namespace Base\Repositories;

use Base\Models\Word;
use Base\Repositories\Interfaces\IWordRepository;

class WordRepository extends BaseRepository implements IWordRepository
{
    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Define used model.
     */
    public static function Model()
    {
        return new Word();
    }

    public function getViewModelName()
    {
        return '\Base\Models\vw_Word';
    }

    public static function getByWordAndLanguageCode($word, $languageCode)
    {
        $resultSet = Word::query()
            ->innerJoin('Base\Models\Language', 'Base\Models\Word.language_id = Base\Models\Language.id')
            ->where('word = :word: AND code = :language_code:', array('word' => $word, 'language_code' => $languageCode))
            ->execute();

        return $resultSet;
    }

    public static function getByWordAndLanguageId($word, $languageId)
    {
        return Word::findFirst(array("(word = '$word' AND language_id = '$languageId')"));
    }

    public static function getSuggestionWordDictionary($word, $languageId)
    {
        if($languageId){
            return Word::find(array("(word LIKE '$word%' AND language_id = '$languageId' AND status_id = 1 AND is_on_dictionary = 1)", "limit" => 10));
        }else{
            return Word::find(array("(word LIKE '$word%' AND status_id = 1 AND is_on_dictionary = 1)", "limit" => 10));
        }
    }
}
