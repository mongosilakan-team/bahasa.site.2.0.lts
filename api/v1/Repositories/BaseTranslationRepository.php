<?php
namespace Base\Repositories;
use Base\Models\BaseTranslation;
use Base\Repositories\Interfaces\IBaseTranslationRepository;
use Phalcon\Mvc\Model\Criteria;

class BaseTranslationRepository implements IBaseTranslationRepository
{

	/**
     * Constructor
     */
    public function __construct() {
       
    }

	/**
	 * Define used model
	 */
	public static function Model(){
		return new BaseTranslation();
	}

	public static function getSingleWord($criteria){
		$result = BaseTranslation::findFirst($criteria);
		return $result;
	}
}