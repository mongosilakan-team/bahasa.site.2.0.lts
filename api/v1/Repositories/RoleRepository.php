<?php
namespace Base\Repositories;
use Base\Models\Role;
use Base\Repositories\Interfaces\IRoleRepository;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use Phalcon\Paginator\Adapter\Model as Paginator;

class RoleRepository extends BaseRepository implements IRoleRepository
{

	/**
     * Constructor
     */
    public function __construct() {

    }

	/**
	 * Define used model
	 */
	public static function Model(){
		return new Role();
	}

	public function getViewModelName() {
        return '\Base\Models\Role';
    }
}
