<?php

namespace Base\Repositories;

use Base\Models\Translation;
use Base\Repositories\Interfaces\ITranslationRepository;

class TranslationRepository extends BaseRepository implements ITranslationRepository
{
    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Define used model.
     */
    public static function Model()
    {
        return new Translation();
    }

    public function getViewModelName()
    {
        return '\Base\Models\vw_ManageTranslation';
    }

    public static function checkExistingTranslation($firstWordId, $secondWordId)
    {
        return Translation::findFirst(array("(first_word_id = '$firstWordId' AND second_word_id = '$secondWordId') OR (first_word_id = '$secondWordId' AND second_word_id = '$firstWordId')"));
    }
}
