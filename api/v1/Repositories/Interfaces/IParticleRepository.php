<?php
namespace Base\Repositories\Interfaces;

interface IParticleRepository extends IBaseRepository  
{
    public static function getByTypeAndLanguageId($type, $language_id);
}