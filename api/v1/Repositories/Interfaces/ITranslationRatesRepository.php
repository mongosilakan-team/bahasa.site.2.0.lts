<?php
namespace Base\Repositories\Interfaces;

interface ITranslationRatesRepository extends IBaseRepository
{
	public static function checkExistingTranslationRates($fwi, $swi, $userId);
}
