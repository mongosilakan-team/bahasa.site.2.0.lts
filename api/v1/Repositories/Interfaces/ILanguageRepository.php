<?php
namespace Base\Repositories\Interfaces;

interface ILanguageRepository extends IBaseRepository
{
    public static function getByCode($code);
    public static function getActive();
    public static function getActiveAndPending();
    public static function getActiveLanguagesAndByIds($ids);
}
