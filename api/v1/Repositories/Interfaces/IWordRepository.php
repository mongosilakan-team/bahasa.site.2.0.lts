<?php
namespace Base\Repositories\Interfaces;

interface IWordRepository extends IBaseRepository
{
	public static function getByWordAndLanguageCode($word, $languageCode);
	public static function getByWordAndLanguageId($word, $languageId);
	public static function getSuggestionWordDictionary($word, $languageId);
}
