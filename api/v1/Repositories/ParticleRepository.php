<?php
namespace Base\Repositories;
use Base\Models\Particle;
use Base\Repositories\Interfaces\IParticleRepository;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use Phalcon\Paginator\Adapter\Model as Paginator;

class ParticleRepository extends BaseRepository implements IParticleRepository
{
    
    /**
     * Define used model
     */
    public static function Model() {
        return new Particle();
    }
    
    public function getViewModelName() {
        return '\Base\Models\Particle';
    }
    
    /**
     *
     */
    public static function getByTypeAndLanguageId($type, $language_id) {
        try {
            $conditions = "type = :type: AND language_id = :language_id:";
            $parameters = array("type" => $type, "language_id" => $language_id);
            $result = Particle::find(array($conditions, "bind" => $parameters, "order" => "order_number",));
            return $result;
        }
        catch(\Exception $e) {
            var_dump($e->getMessage());
            throw $e;
        }
    }
}
