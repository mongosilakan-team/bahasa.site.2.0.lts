{% if mode == "admin" %}
    {{ partial("backend/index") }}
{% elseif mode == "login" %}
    {{ partial("backend/login") }}
{% elseif mode == "public" %}
    {{ partial("frontend/index") }}
{% elseif mode == "donate" %}
    {{ partial("donate") }}
{% else %}
    NOT FOUND!!
{% endif %}
