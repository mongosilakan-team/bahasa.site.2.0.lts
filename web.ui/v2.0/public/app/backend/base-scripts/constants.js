app.constant("constants", {
    "modules": {
        resources: {
            resource: {
                api: "resource/"
            }
        },
        configurations: {
            user: {
                api: "user/"
            },
            status: {
                api: "status/"
            }
        },
        dictionary: {
            word: {
                api: "word/"
            },
            language: {
                api: "language/"
            },
            wordType: {
                api: "wordType/"
            },
            translation: {
                api:"translation/"
            }
        },
        donation: {
            donor: {
                api: "donor/"
            },
            donation: {
                api: "donation/"
            }
        },
        run: {
            run: {
                api: "run/"
            },
        },
        translate: {
            translate: {
                api: "translate/"
            },
        }
    },
    "message_type": {
        success: 'success',
        warning: 'warning',
        error: 'error',
        info: 'info'
    },
    "status": {
        active: '1',
        inactive: '2',
        pending: '3',
        review: '4'
    }
});
