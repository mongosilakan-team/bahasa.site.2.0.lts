var app = angular.module("app", ["angular-loading-bar", "ngRoute", "ui.bootstrap", "ngAnimate", "ngTable", "configurationsModule", "dictionaryModule", "donationModule", "runModule"]);
app.config(['$routeProvider', '$modalProvider', 'cfpLoadingBarProvider',
    function($routeProvider, $modalProvider, cfpLoadingBarProvider) {
        $routeProvider.when("/dashboard", {
            templateUrl: "app/backend/views/dashboard/dashboard.html",
            controller: "dashboardCtrl"
        }).otherwise({
            redirectTo: "/dashboard"
        });
        $modalProvider.options.animation = false;
        cfpLoadingBarProvider.latencyThreshold = 0;
    }
]);
app.run(function(baseCommon) {
    baseCommon.appInit();
});
