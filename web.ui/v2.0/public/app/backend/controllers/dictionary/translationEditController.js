dictionaryModule.controller('translationEditController', translationEditController);

function translationEditController($scope, $routeParams, $location, baseCommon, translationService) {
    baseCommon.clearMessages();
    translationService.getById($routeParams.id).then(function(result) {
        $scope.entity = result.content.model;
    }, function(error) {
        baseCommon.pushMessages(error.content.messages);
    });
    $scope.submitForm = function(isValid) {
        if (isValid) {
            translationService.update($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.dictionary.subs.translations.url);
            }, function(result) {
                console.log("Error Happened");
            }).finally(function() {});
        }
    };
}