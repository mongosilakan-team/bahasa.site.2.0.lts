dictionaryModule.controller('languageEditController', languageEditController);

function languageEditController($scope, $routeParams, $location, baseCommon, languageService) {
    baseCommon.clearMessages();
    languageService.getById($routeParams.id).then(function(result) {
        $scope.entity = result.content.model;
    }, function(error) {
        baseCommon.pushMessages(error.content.messages);
    });
    $scope.submitForm = function(isValid) {
        if (isValid) {
            languageService.update($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.dictionary.subs.languages.url);
            }, function(result) {
                baseCommon.pushMessages(result);
            }).finally(function() {});
        }
    };
}