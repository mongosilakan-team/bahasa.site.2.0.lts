dictionaryModule.controller('wordTypeCreateController', wordTypeCreateController);

function wordTypeCreateController($scope, $location, baseCommon, wordTypeService) {
    baseCommon.clearMessages();
    $scope.submitForm = function(isValid) {
        if (isValid) {
            wordTypeService.create($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.dictionary.subs.wordTypes.url);
            }, function() {
                baseCommon.pushMessages(result.content.messages);
            }).finally(function() {});
        }
    };
}