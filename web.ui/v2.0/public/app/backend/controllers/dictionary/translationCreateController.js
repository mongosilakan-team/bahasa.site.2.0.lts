dictionaryModule.controller('translationCreateController', translationCreateController);

function translationCreateController($scope, $routeParams, $location, baseCommon, translationService) {
    baseCommon.clearMessages();
    $scope.submitForm = function(isValid) {
        $scope.formData = $scope.formTest;
        if (isValid) {
            $scope.entity.translation_type_id = null;
            translationService.create($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.dictionary.subs.translations.url);
            }, function(result) {
                baseCommon.pushMessages(result.content.messages);
            }).finally(function() {});
        }
    };
}