dictionaryModule.controller('wordTypeEditController', wordTypeEditController);

function wordTypeEditController($scope, $routeParams, $location, baseCommon, wordTypeService) {
    baseCommon.clearMessages();
    wordTypeService.getById($routeParams.id).then(function(result) {
        $scope.entity = result.content.model;
    }, function(error) {
        baseCommon.pushMessages(error.content.messages);
    });
    $scope.submitForm = function(isValid) {
        if (isValid) {
            wordTypeService.update($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.dictionary.subs.wordTypes.url);
            }, function(result) {
                console.log("Error Happened");
            }).finally(function() {});
        }
    };
}