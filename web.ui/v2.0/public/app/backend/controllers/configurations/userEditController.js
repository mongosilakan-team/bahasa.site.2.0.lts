configurationsModule.controller('userEditController', userEditController);

function userEditController($scope, $routeParams, $location, baseCommon, userService) {
    baseCommon.clearMessages();
    userService.getById($routeParams.id).then(function(result) {
        $scope.entity = result.content.model;
    }, function(error) {
        baseCommon.pushMessages(error.content.messages);
        $scope.disabled = true;
    });
    $scope.submitForm = function(isValid) {
        $scope.formData = $scope.formTest;
        if (isValid) {
            userService.update($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.configurations.subs.users.url);
            }, function(error) {
                baseCommon.pushMessages(error.content.messages);
            }).
            finally(function() {});
        }
    };
}