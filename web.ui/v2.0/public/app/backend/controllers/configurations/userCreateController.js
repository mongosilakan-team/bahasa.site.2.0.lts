configurationsModule.controller('userCreateController', userCreateController);

function userCreateController($scope, $location, baseCommon, userService) {
    baseCommon.clearMessages();
    $scope.submitForm = function(isValid) {
        if (isValid) {
            userService.create($scope.entity).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.configurations.subs.users.url);
            }, function(error) {
                baseCommon.pushMessages(error.content.messages);
            }).finally(function() {});
        }
    };
}