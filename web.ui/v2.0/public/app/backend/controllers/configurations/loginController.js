app.controller('loginController', loginController);

function loginController($scope, $http, $location,baseCommon, userService) {
    baseCommon.clearMessages();
    $scope.submitForm = function(isValid) {
        $scope.formData = $scope.formTest;
        if (isValid) {
            userService.login($scope.user).then(function(result) {
                if (result.content) {
                    $location.path("dashboard");
                }
            }, function() {
                baseCommon.pushMessages(result.content.messages);
            }).finally(function() {});
        }
    };
}
