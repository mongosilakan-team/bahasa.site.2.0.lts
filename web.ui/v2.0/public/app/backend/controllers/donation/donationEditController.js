donationModule.controller('donationEditController', donationEditController);

function donationEditController($scope, $routeParams, $location, baseCommon, donationService) {
    baseCommon.clearMessages();
    donationService.getById($routeParams.id).then(function(result) {
        $scope.donation = result.content.model;
    }, function(error) {
        baseCommon.pushMessages(error.content.messages);
    });
    $scope.submitForm = function(isValid) {
        $scope.formData = $scope.formTest;
        if (isValid) {
            donationService.update($scope.donation).then(function(result) {
                baseCommon.pushMessages(result.content.messages);
                $location.path($scope.navigation.donation.subs.donations.url);
            }, function(result) {
                baseCommon.pushMessages(result);
            }).finally(function() {});
        }
    };
}