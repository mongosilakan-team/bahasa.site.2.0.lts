dictionaryModule.service('languageService', languageServices);

function languageServices($http, $q, config, constants, baseService) {
    var languageServices = {
        getApi: _getApi,
    }
    languageServices = angular.extend(angular.copy(baseService), languageServices);
    return languageServices;

    function _getApi() {
        return constants.modules.dictionary.language.api;
    }
}