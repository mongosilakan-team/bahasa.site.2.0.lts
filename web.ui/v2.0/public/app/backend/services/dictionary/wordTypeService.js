dictionaryModule.service('wordTypeService', wordTypeServices);

function wordTypeServices($http, $q, config, constants, baseService) {
    var wordTypeServices = {
        getApi: _getApi,
    };
    wordTypeServices = angular.extend(angular.copy(baseService), wordTypeServices);
    return wordTypeServices;

    function _getApi() {
        return constants.modules.dictionary.wordType.api;
    }
}