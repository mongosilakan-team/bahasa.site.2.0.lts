dictionaryModule.service('wordService', wordServices);

function wordServices($http, $q, config, constants, baseService) {
    var wordServices = {
        getApi: _getApi,
    };
    wordServices = angular.extend(angular.copy(baseService), wordServices);
    return wordServices;

    function _getApi() {
        return constants.modules.dictionary.word.api;
    }
}