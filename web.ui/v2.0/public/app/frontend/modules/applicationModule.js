var applicationModule = angular.module('applicationModule', []);
applicationModule.config(function($routeProvider, templateBaseUrl) {
    $routeProvider
        .when("/homepage", {
            templateUrl: templateBaseUrl + "app/frontend/views/homepage.html",
            controller: "homepageController",
            animation: 'first',
            isHomePage: true
        }).when("/translate", {
            templateUrl: templateBaseUrl + "app/frontend/views/translate.html",
            controller: "translateController",
            animation: 'second'
        })
});
