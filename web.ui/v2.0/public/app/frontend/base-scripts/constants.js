app.constant("constants", {
    "modules": {
        resources: {
            resource: {
                api: "resource/"
            }
        },
        dictionary: {
            word: {
                api: "word/"
            },
            language: {
                api: "language/"
            },
            wordType: {
                api: "wordType/"
            },
            translation: {
                api:"translation/"
            }
        },
		application: {
			translate: {
				api: "translate/"
			}
		}
    },
    "message_type": {
        success: 'success',
        warning: 'warning',
        error: 'error',
        info: 'info'
    },
    "status": {
        active: '1',
        inactive: '2',
        pending: '3',
        review: '4'
    },
    // "language":{
    //     1 :
    // }
});
